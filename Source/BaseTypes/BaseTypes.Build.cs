using UnrealBuildTool;

public class BaseTypes : ModuleRules
{
    public BaseTypes(ReadOnlyTargetRules Target) : base(Target)
    {
        PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

        PublicDependencyModuleNames.AddRange(new string[] {
            "Core",
            "InputCore",
            "CoreUObject"
            });
    }
}