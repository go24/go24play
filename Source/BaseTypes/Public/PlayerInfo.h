#pragma once

#include <CoreMinimal.h>
#include <InputCoreTypes.h>
#include <Math/Color.h>

#include "PlayerInfo.generated.h"

UENUM(BlueprintType)
enum class EGameType : uint8
{
    TeamDeathmatch = 0 UMETA(DisplayName = "Deathmatch"),
    BattleRoyale = 1 UMETA(DisplayName = "BattleRoyale")
};

UENUM(BlueprintType)
enum class EDeviceType : uint8
{
    DT_Keyboard UMETA(DisplayName = "keyboard"),
    DT_Joystick UMETA(DisplayName = "joystick"),
    DT_Smartphone UMETA(DisplayName = "smartphone")
};

UENUM(BlueprintType)
enum class EActionType : uint8
{
    EA_Forward UMETA(DisplayName = "forward"),
    EA_Backward UMETA(DisplayName = "backward"),
    EA_Leftward UMETA(DisplayName = "leftward"),
    EA_Rightward UMETA(DisplayName = "rightward"),
    EA_Fire UMETA(DisplayName = "fire")
};

USTRUCT(BlueprintType)
struct FPlayerInfo
{
    GENERATED_BODY()
    
    UPROPERTY(EditAnywhere, BlueprintReadWrite)
    APlayerController* OwnedPlayerController = nullptr;

    UPROPERTY(EditAnywhere, BlueprintReadWrite)
    FString PlayerName = FString("Player");

    UPROPERTY(EditAnywhere, BlueprintReadWrite)
    int32 UniquePlayerId = -1;

    UPROPERTY(EditAnywhere, BlueprintReadWrite)
    int32 TeamId = -1;

    UPROPERTY(EditAnywhere, BlueprintReadWrite)
    int32 PlayerId = -1;

    UPROPERTY(EditAnywhere, BlueprintReadWrite)
    EDeviceType DeviceType = EDeviceType::DT_Joystick;

    UPROPERTY(EditAnywhere, BlueprintReadWrite)
    bool Ready = false;
};

USTRUCT(BlueprintType)
struct FLobbyInfo
{
    GENERATED_BODY()

    UPROPERTY(EditAnywhere, BlueprintReadWrite)
    APlayerController* LeadPlayerController = nullptr;
    
    UPROPERTY(EditAnywhere, BlueprintReadWrite)
    int64 LobbyId = 0;

    UPROPERTY(EditAnywhere, BlueprintReadWrite,  meta = (UIMin = 1))
    int32 MaxTeamNumber = 1;

    UPROPERTY(EditAnywhere, BlueprintReadWrite)
    EGameType GameType = EGameType::TeamDeathmatch;
};
