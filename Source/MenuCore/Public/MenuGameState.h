#pragma once

#include <CoreMinimal.h>
#include <GameFramework/GameStateBase.h>

#include "MenuGameState.generated.h"

/**
 * 
 */
UCLASS()
class MENUCORE_API AMenuGameState : public AGameStateBase
{
	GENERATED_BODY()
	
};
