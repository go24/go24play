// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include <CoreMinimal.h>
#include <GameFramework/GameModeBase.h>

#include "MenuGameMode.generated.h"

/**
 * 
 */
UCLASS()
class MENUCORE_API AMenuGameMode : public AGameModeBase
{
	GENERATED_BODY()
	

public:
	void PreLogin(const FString& Options, const FString& Address, const FUniqueNetIdRepl& UniqueId, FString& ErrorMessage) override;

	void PostLogin(APlayerController* NewPlayer) override;
};
