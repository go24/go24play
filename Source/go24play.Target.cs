using UnrealBuildTool;
using System.Collections.Generic;

public class go24playTarget : TargetRules
{
   public go24playTarget(TargetInfo Target) : base(Target)
   {
      Type = TargetType.Game;

      ExtraModuleNames.AddRange( new string[]
      {
         "BaseTypes",
         "Interfaces",
         "Helpers",
         "InputSystem",
         "TeamColor",
         "Config",
         "MenuCore",
         "MenuMatching",
         "MenuStart",
         "Player",
         "AI",
         "Arena",
         "GameCore",
         "GameInstance",
         "GameViewportClient",
         "go24play",
         "ScreenMessage",
         "GameLiftServerSDK"
      });
   }
}
