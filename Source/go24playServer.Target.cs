using UnrealBuildTool;
using System.Collections.Generic;

public class go24playServerTarget : TargetRules
{
    public go24playServerTarget(TargetInfo Target) : base(Target)
    {
        Type = TargetType.Server;

        ExtraModuleNames.AddRange( new string[] 
        {
            "BaseTypes",
            "Interfaces",
            "Helpers",
            "InputSystem",
            "TeamColor",
            "Config",
            "MenuCore",
            "MenuMatching",
            "MenuStart",
            "Player",
            "AI",
            "Arena",
            "GameCore",
            "GameInstance",
            "GameViewportClient",
            "LobbyCore",
            "go24play",
            "ScreenMessage",
            "GameLiftServerSDK"
        });
    }
}
