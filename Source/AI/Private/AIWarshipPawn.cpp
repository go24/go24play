#include "Public/AIWarshipPawn.h"

void AAIWarshipPawn::BeginPlay()
{
	Super::BeginPlay();
	
	GetWorld()->GetTimerManager().SetTimer(InitializeTimerHandle, this, &AAIWarshipPawn::AIInitialize, DelayInitializeTime, false, DelayInitializeTime);
}

void AAIWarshipPawn::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);

	if (bIsInitialized)
	{
		AIStrafe();
		AIFindMoveLocation();
		AIFindAimLocation();
		AIMoving(LocationToMove);
		AITurnRight(LocationToMove);
		AIFiring();
	}
}

void AAIWarshipPawn::AIInitialize()
{
	TArray<AActor*> ArenaArray;
	UGameplayStatics::GetAllActorsOfClass(GetWorld(), AArenaActor::StaticClass(), ArenaArray);
	AArenaActor* ArenaRef = Cast<AArenaActor>(ArenaArray[0]);

	FBoxSphereBounds SphereBound;
	SphereBound = Mesh->Bounds;

	const float OffsetFromBorders = 1.5f;
	ActorBoundRadius = SphereBound.SphereRadius * OffsetFromBorders;       //TODO : Magic number 1.5 is offset from borders 

	// TODO : get correct arena size
	ArenaExtent = FVector2D((ArenaRef->ArenaPixelSizeTest.X - ActorBoundRadius), (ArenaRef->ArenaPixelSizeTest.Y - ActorBoundRadius));

	TArray<AActor*> WarshipArray;
	UGameplayStatics::GetAllActorsOfClass(GetWorld(), AWarshipPawn::StaticClass(), WarshipArray);

	for (AActor* Target : WarshipArray)
	{
		AWarshipPawn* TargetTmp = Cast<AWarshipPawn>(Target);
		if (IsEnemy(TargetTmp))
		{
			EnemyArray.Add(TargetTmp);
		}
	}

	//PrimaryActorTick.TickInterval = 0.04f;

	GetWorld()->GetTimerManager().SetTimer(UpdateTimerHandle, this, &AAIWarshipPawn::UpdateEnemy, UpdateEnemyTime, true);
	bIsInitialized = true;
 }


void AAIWarshipPawn::AITurnRight(FVector Location)
{
	if (bIsDead) return;

	if (!IsTargetLocationInAim(Location))
	{
		const float DeltaSeconds = UGameplayStatics::GetWorldDeltaSeconds(this);
		float DirDot = FVector::DotProduct(GetDirectionVectorToTarget(Location), GetActorRightVector());
		float Mult = (DirDot > 0) ? 1.0f : (-1.0f);
		float DeltaYaw = DeltaSeconds * Mult * AngularSpeed;
		float CurrentYaw = GetActorRotation().Yaw;
		CurrentYaw += DeltaYaw;
		if (CurrentYaw >= 360)
		{
			CurrentYaw -= 360;
		}
		SetActorRotation(FRotator(0.f, CurrentYaw, 0.f));
	}
}

void AAIWarshipPawn::AIMoving(FVector Location)
{
	if (bIsDead) return;

	const FVector ActorLocation = GetActorLocation();
	float DistanceToLocation = FVector::Dist(Location, ActorLocation);
	// TODO : Maybe refactoring!!! Magic number 10. Need DistanceTolerance to reduce acceleration, when AI is near and near
	float DistanceTolerance = FVector::Dist(FVector((ArenaExtent.X / 10), 0.f, 0.f), FVector(0.f, (ArenaExtent.Y / 10), 0.f));
	float Current = DistanceToLocation / DistanceTolerance;
	float Accel = FMath::FInterpTo(Current, 0.f, GetWorld()->GetDeltaSeconds(), 1.0f);

	MoveForward(Accel);

	bIsArrive = (Location.Equals(GetActorLocation(), ActorBoundRadius * ActorArriveBoundMult)) ? 1 : 0;
}

void AAIWarshipPawn::AIStrafe()
{
	if (bIsDead) return;

	if (!bNeedToStrafe)
	{
		TArray<AActor*> ProjectileArray;
		UGameplayStatics::GetAllActorsOfClass(GetWorld(), AWarshipProjectile::StaticClass(), ProjectileArray);

		for (AActor* Target : ProjectileArray)
		{
			if (!bNeedToStrafe)
			{
				AWarshipProjectile* ProjectileTmp = Cast<AWarshipProjectile>(Target);
				if (IsEnemyProjectile(ProjectileTmp))
				{
					if (IsTargetAiming(ProjectileTmp, true))
					{
						bNeedToStrafe = true;
						EnemyProjectile = ProjectileTmp;
					}
				}
			}
		}
		if (!bIsDelayResetStrafe)
		{
			GetWorld()->GetTimerManager().SetTimer(ResetStrafeTimerHandle, this, &AAIWarshipPawn::AIResetStrafe, DelayResetStrafeTime, false, DelayResetStrafeTime);
			bIsDelayResetStrafe = true;
		}
		
	}
}

void AAIWarshipPawn::AIFiring()
{
	if (bIsDead) return;

	if (EnemyTarget && !EnemyTarget->GetIsDead())
	{
		if (IsTargetLocationInAim(EnemyTarget->GetActorLocation()) || IsTargetLocationInAim(LocationAim))
		{
			Fire();
		}
	}
}

void AAIWarshipPawn::GetLocationToMove(FVector& AsLocationToMove) const
{
	AsLocationToMove = LocationToMove;
}

bool AAIWarshipPawn::IsLocationInsideArena(FVector Location) const
{
	return (Location.X > (-ArenaExtent.X) / 2 && Location.X < ArenaExtent.X / 2 && Location.Y >(-ArenaExtent.Y) / 2 && Location.Y < ArenaExtent.Y / 2) ? 1 : 0;
}

void AAIWarshipPawn::AIFindMoveLocation()
{
	//AIFindRandomLocation();
	
	if (bNeedToStrafe && EnemyProjectile)
	{
		AIFindStrafeLocation();
	}
	else
	{
		if (EnemyTarget && !EnemyTarget->GetIsDead())
		{
			AIFindEnemyLocation(LocationAim);
		}
		else
		{
			AIFindRandomLocation();
		}
	}
	
}

void AAIWarshipPawn::AIFindStrafeLocation()
{
	FVector VelocityDir = EnemyProjectile->GetVelocity().GetSafeNormal();
	float RandomYaw = (UKismetMathLibrary::RandomBool()) ? 90.f : (-90.f);
	FVector RandomLoc = VelocityDir.RotateAngleAxis(RandomYaw, FVector(0, 0, 1));
	LocationToMove = GetActorLocation() + (RandomLoc * ActorBoundRadius*2); 
}

void AAIWarshipPawn::AIFindEnemyLocation(FVector AimLocation)
{
	
	const FVector TargetLocation = EnemyTarget->GetActorLocation();

	if (IsLocationInsideArena(AimLocation))
	{
		const FVector ActorLocation = GetActorLocation();
		float DistanceToTarget = FVector::Dist(TargetLocation, ActorLocation);
		float DistanceToAim = FVector::Dist(AimLocation, ActorLocation);

		LocationToMove = (DistanceToTarget > DistanceToAim) ? TargetLocation : AimLocation;
	}
	else
	{
		LocationToMove = TargetLocation;
	}
	
	//LocationToMove = TargetLocation;
}

void AAIWarshipPawn::AIFindRandomLocation()
{
	if (bIsArrive)
	{
		float RandomX = FMath::FRandRange(ArenaExtent.X / 2, (-ArenaExtent.X) / 2);
		float RandomY = FMath::FRandRange(ArenaExtent.Y / 2, (-ArenaExtent.Y) / 2);
		LocationToMove = FVector(RandomX, RandomY, 0.f);
		bIsArrive = false;
	}
}

void AAIWarshipPawn::AIFindAimLocation()
{
	if (EnemyTarget && !EnemyTarget->GetIsDead())
	{
		const FVector ActorLocation = GetActorLocation();
		const FVector TargetLocation = EnemyTarget->GetActorLocation();

		float DistanceTargetToPrevTick = FVector::Dist(TargetLocation, LocationPrevTick);
		float DistanceActorToPrevTick = (FVector::Dist(ActorLocation, LocationPrevTick) / 30.f);  // TODO : magic number 30 - velocity of bullet in one tick

		FVector DirectionUnitVector = (UKismetMathLibrary::FindLookAtRotation(LocationPrevTick, TargetLocation)).Vector().GetSafeNormal();
		
		FVector DeltaTargetLocation = DirectionUnitVector * (DistanceTargetToPrevTick * DistanceActorToPrevTick);

		FVector TargetAimLocation = TargetLocation + DeltaTargetLocation;

		// TODO : magic number 8
		LocationAim = FMath::VInterpTo(LocationAim, TargetAimLocation, GetWorld()->GetDeltaSeconds(), 8.0f); 

		if (!bIsDelayLocationPrevTick)
		{
			GetWorld()->GetTimerManager().SetTimer(LocationPrevTickTimerHandle, this, &AAIWarshipPawn::AILocationPrevTick, GetWorld()->GetDeltaSeconds(), false, 0.f);
			bIsDelayLocationPrevTick = true;
		}
	}
}

void AAIWarshipPawn::AILocationPrevTick()
{
	if (EnemyTarget && !EnemyTarget->GetIsDead())
	{
		LocationPrevTick = EnemyTarget->GetActorLocation();
		bIsDelayLocationPrevTick = false;
	}
}

void AAIWarshipPawn::UpdateEnemy()
{
	if (bIsDead) return;

	if (EnemyTarget && !EnemyTarget->GetIsDead())
	{
		if (!bIsDelayResetEnemy)
		{
			GetWorld()->GetTimerManager().SetTimer(DelayEnemyTimerHandle, this, &AAIWarshipPawn::EnemyNull, DelayResetEnemyTime,false, DelayResetEnemyTime);
			bIsDelayResetEnemy = true;
		}
	}
	else
	{
		FindEnemy();
	}
}

void AAIWarshipPawn::EnemyNull()
{
	EnemyTarget = nullptr;
	bIsDelayResetEnemy = false;
}

void AAIWarshipPawn::FindEnemy()
{
	float DistanceTmp = 10000.f;
	AWarshipPawn* TargetTmp = nullptr;

	for (AWarshipPawn *Target : EnemyArray)
	{
		const FVector ActorLocation = GetActorLocation();
		const FVector TargetLocation = Target->GetActorLocation();
		float DistanceLocalTmp = FVector::Dist(TargetLocation, ActorLocation);
		if ( !(Target->GetIsDead()) && (DistanceLocalTmp < DistanceTmp))
		{
			DistanceTmp = DistanceLocalTmp;
			TargetTmp = Target;
		}
	}
	if (TargetTmp)
	{
		EnemyTarget = TargetTmp;
		AILocationPrevTick();
	}
}

void AAIWarshipPawn::FindEnemyInSight()
{
	float DistanceTmp = 10000.f;
	AWarshipPawn* TargetTmp = nullptr;
	for (AWarshipPawn * Target : EnemyArray)
	{
		const FVector ActorLocation = GetActorLocation();
		const FVector TargetLocation = Target->GetActorLocation();
		float DistnceLocalTmp = FVector::Dist(TargetLocation, ActorLocation);
		if (!(Target->GetIsDead()) && (DistnceLocalTmp < DistanceTmp) && (IsTargetInSight(Target)))
		{
			DistanceTmp = DistnceLocalTmp;
			TargetTmp = Target;
		}
	}
	if (TargetTmp)
	{
		EnemyTarget = TargetTmp;
		AILocationPrevTick();
	}
	else
	{
		FindEnemy();
	}
}

void AAIWarshipPawn::GetEnemy(AWarshipPawn*& AsEnemy) const
{
	AsEnemy = EnemyTarget;
}

bool AAIWarshipPawn::IsEnemy(AWarshipPawn *Target) const
{
	return ((Target->GetColor()) != GetColor()) ? 1 : 0;
}


void AAIWarshipPawn::GetEnemyProjectile(AWarshipProjectile*& AsEnemyPrijectile) const
{
	AsEnemyPrijectile = EnemyProjectile;
}

bool AAIWarshipPawn::IsEnemyProjectile(AWarshipProjectile *Target) const
{
	return (Target->Color != Color) ? 1 : 0;
}

void AAIWarshipPawn::EnemyProjectileNull()
{
	EnemyProjectile = nullptr;
}


void AAIWarshipPawn::AIResetStrafe()
{
	EnemyProjectileNull();
	bNeedToStrafe = false;
	bIsDelayResetStrafe = false;
}

bool AAIWarshipPawn::IsTargetAiming(AActor* Target, bool bIsVelocityRot) const
{
	
	bool bIsTargetAiming;
	const FVector ActorLocation = GetActorLocation();
	const FVector TargetLocation = Target->GetActorLocation();
	const float DirectionToActorYaw = UKismetMathLibrary::FindLookAtRotation(TargetLocation, ActorLocation).Yaw;
 
	const float VelocityRotYaw = Target->GetVelocity().Rotation().Yaw;
	const float TargetRotYaw = Target->GetActorRotation().Yaw;

	const float AccuracyTolerance = FMath::GetMappedRangeValueUnclamped(InRangeDistance, OutRangeDegree, FVector::Dist(TargetLocation, ActorLocation));
	bIsTargetAiming = FMath::IsNearlyEqual(DirectionToActorYaw, GetActorRotation().Yaw, AccuracyTolerance);
	return bIsTargetAiming;
}

bool AAIWarshipPawn::IsTargetLocationInAim(FVector Location) const
{
	bool bInSight;
	const FVector ActorLocation = GetActorLocation();
	const float DirectionToTargetYaw = UKismetMathLibrary::FindLookAtRotation(ActorLocation, Location).Yaw;
	
	const float AccuracyTolerance = FMath::GetMappedRangeValueUnclamped(InRangeDistance, OutRangeDegree, FVector::Dist(Location, ActorLocation));
	bInSight = FMath::IsNearlyEqual(DirectionToTargetYaw, GetActorRotation().Yaw, AccuracyTolerance);
	return bInSight;
}

FVector AAIWarshipPawn::GetDirectionVectorToTarget(FVector Location) const
{
	const FVector ActorLocation = GetActorLocation();
	const FRotator LookRot = UKismetMathLibrary::FindLookAtRotation(ActorLocation, Location);
	FVector DirectionVector = LookRot.Vector();
	return DirectionVector;
}

bool AAIWarshipPawn::IsTargetInSight(AWarshipPawn* Target) const
{
	const float DotToTarget = FVector::DotProduct(Target->GetActorForwardVector(), GetActorForwardVector());
	FRotator SightRot(0.f, HalfOfAngleSight, 0.f);
	FVector DirectionVector = SightRot.Vector();
	const float DotSight = FVector::DotProduct(GetActorForwardVector(), DirectionVector);
	return (DotToTarget > DotSight) ? 1 : 0;
}

