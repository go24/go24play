// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include <CoreMinimal.h>
#include "PlayerPawn/WarshipPawn.h"
#include "PlayerProjectile/WarshipProjectile.h"
#include "Arena/Private/ArenaActor.h"

#include "TimerManager.h"
#include <Kismet/GameplayStatics.h>
#include "Kismet/KismetMathLibrary.h"
//#include "Math/UnrealMathUtility.h"
#include "Math/Vector.h"
#include "Math/Vector2D.h"
#include "Math/Rotator.h"

#include "AIWarshipPawn.generated.h"

UCLASS()
class AI_API AAIWarshipPawn : public AWarshipPawn
{
    GENERATED_BODY()

        FTimerHandle InitializeTimerHandle;
        FTimerHandle UpdateTimerHandle;     
        FTimerHandle DelayEnemyTimerHandle;
        FTimerHandle ResetStrafeTimerHandle;
        FTimerHandle LocationPrevTickTimerHandle;
        bool bIsDelayResetEnemy = false;
        bool bIsDelayLocationPrevTick = false;
        bool bIsDelayResetStrafe = false;   

        //   Initialization variables
        TArray<AWarshipPawn*> EnemyArray;
        float ActorBoundRadius = 1.f;
        FVector2D ArenaExtent = FVector2D(0.f, 0.f);
        bool bIsInitialized = false;

        // RunTime variables
        bool bIsArrive = true;
        bool bNeedToStrafe = false;
        FVector LocationPrevTick = FVector(0.f, 0.f, 0.f);
        FVector LocationAim = FVector(0.f, 0.f, 0.f);
        FVector LocationToMove = FVector(0.f, 0.f, 0.f);

        AWarshipPawn* EnemyTarget = nullptr;
        AWarshipProjectile* EnemyProjectile = nullptr;

protected:
    virtual void BeginPlay() override;

    virtual void Tick(float DeltaSeconds) override;

public:

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "WarshipPawn|AI")
    float DelayInitializeTime = 2.f;
    
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "WarshipPawn|AI")
    float UpdateEnemyTime = 1.f;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "WarshipPawn|AI")
    float DelayResetEnemyTime = 5.f;
    
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "WarshipPawn|AI")
    float DelayResetStrafeTime = 0.5f;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "WarshipPawn|AI")
    float ActorArriveBoundMult = 5.f;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "WarshipPawn|AI")
    float HalfOfAngleSight = 60.f;

    /** Distance from actor to target */
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "WarshipPawn|AI|Accuracy")
    FVector2D InRangeDistance = FVector2D(0.f, 1000.f);

    /** Map unranged value to tolerance of degrees of angle to aim */
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "WarshipPawn|AI|Accuracy")
    FVector2D OutRangeDegree = FVector2D(0.5f, 0.4f);

public:
    
    void AIInitialize();

    UFUNCTION(BlueprintCallable, Category = "WarshipPawn|AI")
    void AITurnRight(FVector Location);

    UFUNCTION(BlueprintCallable, Category = "WarshipPawn|AI")
    void AIMoving(FVector Location);

    UFUNCTION(BlueprintCallable, Category = "WarshipPawn|AI")
    void AIStrafe();

    UFUNCTION(BlueprintCallable, Category = "WarshipPawn|AI")
    void AIFiring();

    UFUNCTION(BlueprintCallable, Category = "WarshipPawn|AI|LocatioToMove")
    void GetLocationToMove(FVector& AsLocationToMove) const;

    UFUNCTION(BlueprintCallable, Category = "WarshipPawn|AI|LocatioToMove")
    bool IsLocationInsideArena(FVector Location) const;
    
    UFUNCTION(BlueprintCallable, Category = "WarshipPawn|AI|LocatioToMove")
    void AIFindMoveLocation();

    UFUNCTION(BlueprintCallable, Category = "WarshipPawn|AI|LocatioToMove")
    void AIFindStrafeLocation();

    UFUNCTION(BlueprintCallable, Category = "WarshipPawn|AI|LocatioToMove")
    void AIFindEnemyLocation(FVector AimLocation);


    UFUNCTION(BlueprintCallable, Category = "WarshipPawn|AI|LocatioToMove")
    void AIFindRandomLocation();

    UFUNCTION(BlueprintCallable, Category = "WarshipPawn|AI|LocatioToMove")
    void AIFindAimLocation();

    void AILocationPrevTick();

    UFUNCTION(BlueprintCallable, Category = "WarshipPawn|AI|Enemy")
    void UpdateEnemy();

    UFUNCTION(BlueprintCallable, Category = "WarshipPawn|AI|Enemy")
    void EnemyNull();

    UFUNCTION(BlueprintCallable, Category = "WarshipPawn|AI|Enemy")
    void FindEnemy();

    UFUNCTION(BlueprintCallable, Category = "WarshipPawn|AI|Enemy")
    void FindEnemyInSight();


    UFUNCTION(BlueprintCallable, Category = "WarshipPawn|AI|Enemy")
    void GetEnemy(AWarshipPawn* &AsEnemy) const;
    
    UFUNCTION(BlueprintCallable, Category = "WarshipPawn|AI|Enemy")
    bool IsEnemy(AWarshipPawn* Target) const;

    UFUNCTION(BlueprintCallable, Category = "WarshipPawn|AI|EnemyProjectile")
    void GetEnemyProjectile(AWarshipProjectile* &AsEnemyPrijectile) const;

    UFUNCTION(BlueprintCallable, Category = "WarshipPawn|AI|EnemyProjectile")
    bool IsEnemyProjectile(AWarshipProjectile *Target) const;

    UFUNCTION(BlueprintCallable, Category = "WarshipPawn|AI|EnemyProjectile")
    void EnemyProjectileNull();

    
    //UFUNCTION(BlueprintCallable, Category = "WarshipPawn|AI|Misc")
    void AIResetStrafe();

    UFUNCTION(BlueprintCallable, Category = "WarshipPawn|AI|Misc")
    bool IsTargetAiming(AActor* Target, bool bIsVelocityRot) const;

    UFUNCTION(BlueprintCallable, Category = "WarshipPawn|AI|Misc")
    bool IsTargetLocationInAim(FVector Location) const;

    UFUNCTION(BlueprintCallable, Category = "WarshipPawn|AI|Misc")
    FVector GetDirectionVectorToTarget(FVector Location) const;

    UFUNCTION(BlueprintCallable, Category = "WarshipPawn|AI|Misc")
    bool IsTargetInSight(AWarshipPawn* Target) const;

    


    
};
