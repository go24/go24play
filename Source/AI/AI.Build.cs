using UnrealBuildTool;

public class AI : ModuleRules
{
    public AI(ReadOnlyTargetRules Target) : base(Target)
    {
        PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

        bEnableUndefinedIdentifierWarnings = false;

        PublicIncludePaths.AddRange(new string[] 
        {
            "AI/Public"
        });

        PrivateIncludePaths.AddRange(new string[]
        {
            "AI/Private"
        });

        PublicDependencyModuleNames.AddRange(new string[]
        {
            "AI",
            "Arena",
            "Player",
            "Config",
            "GameCore",
            "GameInstance",
            "Interfaces"
        });

        PrivateDependencyModuleNames.AddRange(new string[]
        {
            "Arena",
            "Core",
            "CoreUObject",
            "Engine",
            "UMG"

        });
    }
}