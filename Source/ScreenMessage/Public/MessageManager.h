// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include <CoreMinimal.h>
#include <UObject/UObjectGlobals.h>

#include "ScreenMessageInterface.h"
#include "MessageManager.generated.h"



UCLASS()
class UMessageManager : public UObject, public IScreenMessageInterface
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	UMessageManager(const FObjectInitializer& ObjectInitializer);

protected:

public:	

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "EndPlay")
		void ShowScreenMessage(TArray<FColorfulText> const& message);
	void ShowScreenMessage_Implementation(TArray<FColorfulText> const& message) override;

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "EndPlay")
		void ClearScreenMessage();
	void ClearScreenMessage_Implementation() override;

	UPROPERTY(BlueprintReadWrite , EditAnywhere , Category = "EndPlay")
	TSubclassOf<class UMessageWidget> MessageWidgetClass;

private:

	class UMessageWidget* MessageWidget;

	

};
