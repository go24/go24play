#pragma once
#include "Interface.h"
#include "Math/Color.h"
#include "ColorfulText.h"
#include "ScreenMessageInterface.generated.h"



UINTERFACE(BlueprintType)
class SCREENMESSAGE_API UScreenMessageInterface : public UInterface
{	
	GENERATED_UINTERFACE_BODY()
};

class SCREENMESSAGE_API IScreenMessageInterface
{
	
	GENERATED_IINTERFACE_BODY()

public:


	UFUNCTION(BlueprintNativeEvent , BlueprintCallable , Category = "EndPlay")
		void ShowScreenMessage(TArray<FColorfulText> const& Message);

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable , Category = "EndPlay")
		void ClearScreenMessage();

	
};