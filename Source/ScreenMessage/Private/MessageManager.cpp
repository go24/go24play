// Fill out your copyright notice in the Description page of Project Settings.


#include "MessageManager.h"
#include "MessageWidget.h"

// Sets default values
UMessageManager::UMessageManager(const FObjectInitializer& ObjectInitializer)
	:Super(ObjectInitializer)
{

}

void UMessageManager::ShowScreenMessage_Implementation(TArray<FColorfulText> const& message)
{
#ifdef WITH_EDITOR
	if (!MessageWidgetClass)
	{
		UE_LOG(LogTemp, Warning, TEXT("Forgot to set MessageWidget class inside MessageManager class"));
		return;
	}
#endif // WITH_EDITOR


	if (!MessageWidget)
	{
		MessageWidget = CreateWidget<UMessageWidget>(GetWorld() , MessageWidgetClass.Get());
	}
	MessageWidget->Init(message);
	MessageWidget->AddToViewport();
}

void UMessageManager::ClearScreenMessage_Implementation()
{
	if (MessageWidget)
	{
		MessageWidget->RemoveFromViewport();
	}
}
