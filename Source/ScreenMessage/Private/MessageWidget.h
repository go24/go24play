// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "ColorfulText.h"
#include "MessageWidget.generated.h"

/**
 * 
 */
UCLASS()
class UMessageWidget : public UUserWidget
{
	GENERATED_BODY()
	
public:

	UFUNCTION(BlueprintNativeEvent , BlueprintCallable , Category = "EndPlay")
	void Init(TArray<FColorfulText> const& message);

};
