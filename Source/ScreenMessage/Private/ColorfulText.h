#pragma once

#include <CoreMinimal.h>
#include <InputCoreTypes.h>
#include <Math/Color.h>

#include "ColorfulText.generated.h"


USTRUCT(BlueprintType)
struct SCREENMESSAGE_API FColorfulText
{
	GENERATED_BODY()

		UPROPERTY(BlueprintReadWrite, EditAnywhere)
		FString Text;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
		FLinearColor Color;

};
