using UnrealBuildTool;

public class TeamColor : ModuleRules
{
    public TeamColor(ReadOnlyTargetRules Target) : base(Target)
    {
        PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

        PublicIncludePaths.AddRange(
            new string[] {
            });

        PublicDependencyModuleNames.AddRange(new string[] {
            "CoreUObject",
            "Engine",
            "Core"
            });
    }
}