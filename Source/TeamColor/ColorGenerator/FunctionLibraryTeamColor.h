// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Kismet/BlueprintFunctionLibrary.h"
#include "FunctionLibraryTeamColor.generated.h"

/**
 * 
 */
UCLASS()
class TEAMCOLOR_API UFunctionLibraryTeamColor : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()
	
};
