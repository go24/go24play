#pragma once
#include "GameFramework/Actor.h"
#include "CoreMinimal.h"
#include "ColorGenerator.generated.h"

UCLASS()
class TEAMCOLOR_API AColorGenerator: public AActor
{
    GENERATED_BODY()

public:
    UFUNCTION(BlueprintCallable)
    TArray<FColor> GenerateTeamColors(int32 TeamQuantity);

    UFUNCTION(BlueprintCallable)
    int32  MatchTeamIndexBy(const FColor& TeamColor);

    UFUNCTION(BlueprintCallable)
    FColor MatchColorBy(int32 TeamIndex);

protected:
    UFUNCTION(BlueprintCallable)
    void AddColor(int32 TeamIndex, const FColor& TeamColor);

protected:
    TMap<int32, FColor> MapIndexForColor;
    TMap<FColor, int32> MapColorForIndex;

    bool IsInitialized = false;
};