#include "ColorGenerator.h"
#include "Kismet/KismetMathLibrary.h"

TArray<FColor> AColorGenerator::GenerateTeamColors(int32 TeamQuantity)
{
    MapIndexForColor.Empty();
    MapColorForIndex.Empty();

    TArray<FColor> TeamColors;
    const float HSVMaxValue = 360.f;
    const float ColorStep = HSVMaxValue / TeamQuantity;
    for (int32 ColorIndex = 0; ColorIndex < TeamQuantity; ++ColorIndex)
    {
        const float HSVCurrentValue = ColorIndex * ColorStep;
        const FLinearColor LinearColor = UKismetMathLibrary::HSVToRGB(HSVCurrentValue, 1, 1, 1);
        const FColor CurrentColor = LinearColor.ToRGBE();
        TeamColors.Add(CurrentColor);
        AddColor(ColorIndex, CurrentColor);
    }
    if (TeamQuantity == TeamColors.Num())
        IsInitialized = true;
    return TeamColors;
}

int32 AColorGenerator::MatchTeamIndexBy(const FColor& TeamColor)
{
    if (IsInitialized)
        return MapColorForIndex[TeamColor];
    return 0;
}

FColor AColorGenerator::MatchColorBy(int32 TeamIndex)
{
    auto It = MapIndexForColor.Find(TeamIndex);
    if (It)
    {
        return *It;
    }
        
    return FColor(255);
}

void AColorGenerator::AddColor(int32 TeamIndex, const FColor& TeamColor)
{
    MapIndexForColor.Add(TeamIndex, TeamColor);
    MapColorForIndex.Add(TeamColor, TeamIndex);
}