using UnrealBuildTool;

public class Interfaces : ModuleRules
{
    public Interfaces(ReadOnlyTargetRules Target) : base(Target)
    {
        PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

        PublicIncludePaths.AddRange(new string[] {
            "Interfaces"
        });

        PublicDependencyModuleNames.AddRange(new string[] {
            "Core",
            "CoreUObject",
            "BaseTypes",
            "Config"
        });
    }
}