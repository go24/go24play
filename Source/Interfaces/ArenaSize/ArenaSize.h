#pragma once
#include "Interface.h"
#include "ArenaSize.generated.h"


// (Algerd) ToDo: Where we use it ? and why we will need it ?
UINTERFACE(BlueprintType)
class INTERFACES_API UArenaSize : public UInterface
{
	GENERATED_UINTERFACE_BODY()
};

class INTERFACES_API IArenaSize
{
	GENERATED_IINTERFACE_BODY()

public:
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "ArenaSize")
	FVector2D GetArenaSize();
};