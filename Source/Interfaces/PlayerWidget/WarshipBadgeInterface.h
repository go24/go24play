#pragma once
#include <Interface.h>
#include "WarshipBadgeInterface.generated.h"

UINTERFACE(BlueprintType)
class INTERFACES_API UWarshipBadgeInterface : public UInterface
{
	GENERATED_UINTERFACE_BODY()
};

class INTERFACES_API IWarshipBadgeInterface
{
	GENERATED_IINTERFACE_BODY()

public:
    UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "BadgeInterface")
	void SetHealth(const float Health);

    UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "BadgeInterface")
	void SetId(const uint8 Id);

    UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "BadgeInterface")
	void OnReloading();

    UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "BadgeInterface")
	void OffReloading();

    UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "BadgeInterface")
	void PlayHit(const float DamageValue, const FColor EnemyColor);

    UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "BadgeInterface")
	void PlayHeal(const float DamageValue, const FColor HealerColor);
};
