#include "WarshipBadge.h"

void UWarshipBadge::SetHealth_Implementation(const float Health)
{
}

void UWarshipBadge::SetId_Implementation(const uint8 Id)
{
}

void UWarshipBadge::OnReloading_Implementation()
{
}

void UWarshipBadge::OffReloading_Implementation()
{
}

void UWarshipBadge::PlayHit_Implementation(const float DamageValue, const FColor EnemyColor)
{
}

void UWarshipBadge::PlayHeal_Implementation(const float DamageValue, const FColor HealerColor)
{
}
