#pragma once

#include "CoreMinimal.h"
#include "Components/WidgetComponent.h"
#include "BadgeWidgetComponent.generated.h"

UCLASS(Blueprintable, ClassGroup = "UserInterface", hidecategories = (Object, Activation, "Components|Activation", Sockets, Base, Lighting, LOD, Mesh), editinlinenew, meta = (BlueprintSpawnableComponent))
class PLAYER_API UBadgeWidgetComponent : public UWidgetComponent
{
	GENERATED_BODY()
};
