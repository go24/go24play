#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "PlayerWidget/WarshipBadgeInterface.h"
#include "WarshipBadge.generated.h"

UCLASS(ClassGroup = "UserInterface", Abstract, editinlinenew, BlueprintType, Blueprintable, meta = (DontUseGenericSpawnObject = "True", DisableNativeTick))
class PLAYER_API UWarshipBadge : public UUserWidget, public IWarshipBadgeInterface
{
	GENERATED_BODY()

public:
    UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "WarshipBadge")
    void SetHealth(const float Health);
    virtual void SetHealth_Implementation(const float Health) override;
    
    UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "WarshipBadge")
    void SetId(const uint8 Id);
    virtual void SetId_Implementation(const uint8 Id);

    UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "WarshipBadge")
    void OnReloading();
    virtual void OnReloading_Implementation();

    UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "WarshipBadge")
    void OffReloading();
    virtual void OffReloading_Implementation();

    UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "WarshipBadge")
    void PlayHit(const float DamageValue, const FColor EnemyColor);
    virtual void PlayHit_Implementation(const float DamageValue, const FColor EnemyColor);

    UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "WarshipBadge")
    void PlayHeal(const float DamageValue, const FColor HealerColor);
    virtual void PlayHeal_Implementation(const float DamageValue, const FColor HealerColor);
};
