#pragma once
#include <CoreMinimal.h>
#include <Camera/PlayerCameraManager.h>

#include "WarshipCameraManager.generated.h"

UCLASS()
class PLAYER_API AWarshipCameraManager : public APlayerCameraManager
{
	GENERATED_BODY()

public:
	AWarshipCameraManager();

	UFUNCTION(BlueprintCallable)
	void RemoveDefaultCamera();

protected:
	virtual void BeginPlay() override;
};