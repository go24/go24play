#include "WarshipCameraManager.h"

#include <Engine/World.h>
#include <Camera/CameraActor.h>

AWarshipCameraManager::AWarshipCameraManager()
{
}

void AWarshipCameraManager::RemoveDefaultCamera()
{
    AActor* CurrentAnimCamera = Cast<AActor>(AnimCameraActor);
    if (CurrentAnimCamera)
        GetWorld()->RemoveActor(CurrentAnimCamera, true);
}

void AWarshipCameraManager::BeginPlay()
{
}