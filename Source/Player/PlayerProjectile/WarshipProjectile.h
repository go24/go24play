#pragma once

#include <CoreMinimal.h>
#include <GameFramework/Actor.h>
#include <GameFramework/ProjectileMovementComponent.h>

#include "WarshipProjectile.generated.h"

UCLASS()
class PLAYER_API AWarshipProjectile : public AActor
{
    GENERATED_BODY()

public:
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Projectile")
    UStaticMeshComponent* Mesh;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Projectile")
    UProjectileMovementComponent* ProjectileMovement;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Projectile")
    float Energy;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Projectile")
    float StartEnergy = 10.f;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Projectile")
    float MaxEnergy = 100.f;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Projectile")
    float DowngrageValue = 20.f;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Projectile")
    float ImblazeTimer = 0.5f;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Projectile")
    float BlinkOffTimer = 10.f;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Projectile")
    FColor Color;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Projectile")
    bool IsEnabledDeadProjectile = false;

public:
    AWarshipProjectile();

    UFUNCTION(BlueprintCallable)
    void SetColorImblaze(const FColor& InColor);

    UFUNCTION(BlueprintCallable)
    void ImblazeEnergy();

    UFUNCTION(BlueprintCallable)
    void OnHit(UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit);

    UFUNCTION(blueprintCallable)
    void BlinkOff();

    UFUNCTION(blueprintCallable)
    FColor GetColor() const { return ImblazeColor; }


protected:
    virtual void BeginPlay() override;

    virtual void Tick(float DeltaTime) override;

    virtual void Downgrade();

protected:
    DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnBulletOpening);
    UPROPERTY(BlueprintAssignable, Category = "WarshipPawn|Events")
    FOnBulletOpening OnBulletOpening;

    DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnReflection);
    UPROPERTY(BlueprintAssignable, Category = "WarshipPawn|Events")
    FOnReflection OnReflection;

    DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnOutOfBoard);
    UPROPERTY(BlueprintAssignable, Category = "WarshipPawn|Events")
    FOnOutOfBoard OnOutOfBoard;

private:
    FPointDamageEvent ProjectileDamageEvent;
    FTimerHandle      ImblazeTimerHandle;
    FTimerHandle      BlinkOffTimerHandle;
    FColor            ImblazeColor;
};
