#include "WarshipProjectile.h"
#include "PlayerPawn/WarshipPawn.h"

#include <ConstructorHelpers.h>
#include <ADPCMAudioInfo.h>
#include "GodConfig.h"

AWarshipProjectile::AWarshipProjectile()
{
    Energy = StartEnergy;

    Mesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("ProjectileMesh"));
    Mesh->SetSimulatePhysics(false);
    Mesh->SetWorldScale3D(FVector(0.1f));
    Mesh->SetConstraintMode(EDOFMode::XYPlane);
    Mesh->SetMassOverrideInKg(NAME_None, 1.f);
    Mesh->SetLinearDamping(0.01f);
    Mesh->SetAngularDamping(0.f);
    RootComponent = Mesh;
    Mesh->SetNotifyRigidBodyCollision(true);
    Mesh->OnComponentHit.AddDynamic(this, &AWarshipProjectile::OnHit);
    ProjectileMovement = CreateDefaultSubobject< UProjectileMovementComponent>(TEXT("ProjectileMovementComponent"));
    ProjectileMovement->InitialSpeed = UGodConfig::Get().DesignerRow.EnergySetup.InitialSpeed;
    ProjectileMovement->MaxSpeed = UGodConfig::Get().DesignerRow.EnergySetup.MaxSpeed;
    ProjectileMovement->Bounciness = 1.f;
    
}

void AWarshipProjectile::BeginPlay()
{
    Super::BeginPlay();

    IProjectGameInstanceInterface* ProjectGameInstance = Cast<IProjectGameInstanceInterface>(GetGameInstance());

    ImblazeTimer = UGodConfig::Get().DesignerRow.EnergySetup.ProjectileSetup.ImblazeTimer;
	BlinkOffTimer = UGodConfig::Get().DesignerRow.EnergySetup.ProjectileSetup.ProjectileDieTimer;
	MaxEnergy = UGodConfig::Get().DesignerRow.EnergySetup.ProjectileSetup.MaxEnergy;
	DowngrageValue = UGodConfig::Get().DesignerRow.EnergySetup.ProjectileSetup.DowngrageValue;
	Energy = UGodConfig::Get().DesignerRow.EnergySetup.ProjectileSetup.StartEnergy;

    ProjectileMovement->InitialSpeed = UGodConfig::Get().DesignerRow.EnergySetup.InitialSpeed;
    ProjectileMovement->MaxSpeed = UGodConfig::Get().DesignerRow.EnergySetup.MaxSpeed;

    const float LinearDamping = UGodConfig::Get().DesignerRow.EnergySetup.EnergyPhysicsSetup.LinearDamping;
    const float AngularDamping = UGodConfig::Get().DesignerRow.EnergySetup.EnergyPhysicsSetup.AngularDamping;
    const float MassInKg = UGodConfig::Get().DesignerRow.EnergySetup.EnergyPhysicsSetup.MassInKg;
    const float Bounciness = UGodConfig::Get().DesignerRow.EnergySetup.EnergyPhysicsSetup.Bounciness;

    Mesh->SetAngularDamping(AngularDamping);
    Mesh->SetLinearDamping(LinearDamping);
    Mesh->SetMassOverrideInKg(NAME_None, MassInKg);
    ProjectileMovement->Bounciness = Bounciness;

    UWorld* World = GetWorld();
    if (World)
    {
        World->GetTimerManager().SetTimer(ImblazeTimerHandle, this, &AWarshipProjectile::ImblazeEnergy, ImblazeTimer, false);
        World->GetTimerManager().SetTimer(BlinkOffTimerHandle, this, &AWarshipProjectile::BlinkOff, BlinkOffTimer, false);
    }
}

void AWarshipProjectile::Tick(float DeltaTime)
{
    Super::Tick(DeltaTime);
}

void AWarshipProjectile::SetColorImblaze(const FColor& InColor)
{

    ImblazeColor = InColor;
}

void AWarshipProjectile::ImblazeEnergy()
{
    Energy = MaxEnergy;
    Color = ImblazeColor;
    this->OnBulletOpening.Broadcast();
    const FVector ColorVector(Color.R, Color.G, Color.B);
    Mesh->SetVectorParameterValueOnMaterials("color", ColorVector);
    
}

void AWarshipProjectile::OnHit(UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit)
{
    UE_LOG(LogTemp, Warning, TEXT("HitEvent in Projectile !!!"));

    AWarshipPawn* WarshipPawn = Cast<AWarshipPawn>(OtherActor);
    if (WarshipPawn)
    {
        const FColor WarshipColor = WarshipPawn->GetColor();
        
        if (AWarshipProjectile::Color != WarshipColor)
        {
            AActor* Owner = GetOwner();
            WarshipPawn->TakeDamage(Energy, ProjectileDamageEvent, nullptr, Owner);
            Destroy(true);
        }
        else
        {
            OnOutOfBoard.Broadcast();
        }
    }
    else
    {
        OnReflection.Broadcast();
        Downgrade();
    }
}

void AWarshipProjectile::BlinkOff()
{
    if (IsEnabledDeadProjectile)
    {
        Mesh->SetMassOverrideInKg(NAME_None, 3000.f);
    }
    else
    {
        Destroy();
    }
}

void AWarshipProjectile::Downgrade()
{
    Energy -= DowngrageValue;
    
    if (Energy < 0)
        Destroy();
}
