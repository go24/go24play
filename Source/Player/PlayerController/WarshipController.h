#pragma once

#include <GameFramework/PlayerController.h>
#include <GameFramework/PlayerStart.h>

#include "PlayerPawn/WarshipPawn.h"
#include "WarshipController.generated.h"

UENUM(BlueprintType)
enum class EInputType : uint8
{
	Undefined,
	Keyboard,
	Gamepad,
    Touch
};

UCLASS()
class PLAYER_API AWarshipController : public APlayerController
{
    GENERATED_BODY()

public:
    AWarshipController(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get());

    UFUNCTION(Client, Reliable)
	void Client_SendBackYourClientID();

    UFUNCTION(Server, Reliable)
	void Server_TakeClientID(int32 PlayerID);

    void SeamlessTravelTo(class APlayerController* NewPC) override;

    void SeamlessTravelFrom(class APlayerController* OldPC) override;

    virtual void BeginPlay() override;
    virtual void Tick(float DeltaSeconds) override;

    void SetupInputComponent();

    template <EInputType T>
    void MoveForward(float Value)
    {
        MoveForward(Value, T);
    }
    UFUNCTION(BlueprintCallable)
    void MoveForward(float Value, EInputType InputType);

    template <EInputType T>
    void TurnRight(float Value)
    {
        TurnRight(Value, T);
    }

    void TurnRight(float Value, EInputType InputType);
    void SetTurnAxisX(float Value);
    void SetTurnAxisY(float Value);
    void TurnToVector(FVector InDesiredMovementVector);
    UFUNCTION(BlueprintCallable)
    void Action();
    void ActionTouchPrivate(const ETouchIndex::Type FingerIndex, const FVector Location);

protected:
    void OnPossess(APawn* aPawn) override;

private:
    UPROPERTY(Replicated)
    AWarshipPawn* OwnedWarship = nullptr;
    FVector DesiredMovementVector = FVector::ZeroVector;
    float DeltaAngleBetweenWarshipAndGamepadNotAffectedRotation;
    float ActionTouchForGamePercent;
};
