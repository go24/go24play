
#include "WarshipController.h"
#include "PlayerCameraManager/WarshipCameraManager.h"
#include "PlayerPawn/WarshipPawn.h"
#include "Config/Public/GodConfig.h"
#include "ProjectGameInstance.h"
#include "SatanGameMode.h"

#include <GameFramework/GameModeBase.h>
#include <Kismet/GameplayStatics.h>
#include <Engine/GameEngine.h>
#include <Net/UnrealNetwork.h>

AWarshipController::AWarshipController(const FObjectInitializer& ObjectInitializer)
    : Super(ObjectInitializer)
{
    bAutoManageActiveCameraTarget = false;

}

void AWarshipController::GetLifetimeReplicatedProps(TArray<FLifetimeProperty> & OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

    DOREPLIFETIME(AWarshipController, OwnedWarship);
}

void AWarshipController::Client_SendBackYourClientID_Implementation()
{
    UProjectGameInstance* ProjectGameInstance = Cast<UProjectGameInstance>(GetWorld()->GetGameInstance());
    Server_TakeClientID(ProjectGameInstance->GetUniqueClientID());
}

void AWarshipController::Server_TakeClientID_Implementation(int32 PlayerID)
{
    ASatanGameMode* GameMode = Cast<ASatanGameMode>(GetWorld()->GetAuthGameMode());
    if (GameMode)
    {
        GameMode->ClientIsReadyToSpawn(this, PlayerID);
    }
}

void AWarshipController::OnPossess(APawn* aPawn)
{
    Super::OnPossess(aPawn);
    UE_LOG(LogTemp, Warning, TEXT("OnPossess event"));
    OwnedWarship = Cast<AWarshipPawn>(aPawn);
}

void AWarshipController::BeginPlay()
{
    Super::BeginPlay();

    TArray<AActor*> ArenaActors;
    UWorld* World = GetWorld();
    if (!World)
        return;
	
    UGameplayStatics::GetAllActorsOfClass(World, AActor::StaticClass(), ArenaActors);
    if (ArenaActors.Num())
    {
        for (auto& ArenaActor : ArenaActors)
        {
            if (ArenaActor->ActorHasTag("ArenaActor"))
            {
                SetViewTarget(ArenaActor);

                AWarshipCameraManager* CameraManager = Cast<AWarshipCameraManager>(PlayerCameraManager);
                if (CameraManager)
                {
                    CameraManager->RemoveDefaultCamera();
                }
                else
                {
                    UE_LOG(LogTemp, Warning, TEXT("Forgot to set ModeratorCameraActorClass inside PlayerController_WarShip"));
                }
                break;
            }
        }

    }
    else
    {
        UE_LOG(LogTemp, Warning, TEXT("Forgot to place ASatanCameraActor to World"));
    }

    DeltaAngleBetweenWarshipAndGamepadNotAffectedRotation = UGodConfig::Get().DesignerRow.GamepadSetup.DeltaAngleBetweenWarshipAndGamepadNotAffectedRotation;
    ActionTouchForGamePercent = UGodConfig::Get().DesignerRow.TouchSetup.ActionTouchForGamePercent;
}

void AWarshipController::Tick(float DeltaSeconds)
{
    Super::Tick(DeltaSeconds);

    if (OwnedWarship && !DesiredMovementVector.IsNearlyZero())
    {
        TurnToVector(DesiredMovementVector);
    }
}

void AWarshipController::SeamlessTravelTo(class APlayerController* NewPC)
{
    AGameModeBase* mode = Cast<AGameModeBase>(GetWorld()->GetAuthGameMode());

    Super::SeamlessTravelTo(NewPC);
}

void AWarshipController::SeamlessTravelFrom(class APlayerController* OldPC)
{
    AGameModeBase* mode = Cast<AGameModeBase>(GetWorld()->GetAuthGameMode());

    Super::SeamlessTravelFrom(OldPC);
}

void AWarshipController::SetupInputComponent()
{
    Super::SetupInputComponent();

    //common input actions
    InputComponent->BindAction("Action", IE_Pressed, this, &AWarshipController::Action);
    //keyboard
    InputComponent->BindAxis("MoveForwardKeyboard", this, &AWarshipController::MoveForward<EInputType::Keyboard>);
    InputComponent->BindAxis("TurnRightKeyboard", this, &AWarshipController::TurnRight<EInputType::Keyboard>);
    //gamepad
    InputComponent->BindAxis("RotateAxisX", this, &AWarshipController::SetTurnAxisX);
    InputComponent->BindAxis("RotateAxisY", this, &AWarshipController::SetTurnAxisY);
#if (PLATFORM_ANDROID || PLATFORM_IOS)
    //InputComponent->BindTouch(EInputEvent::IE_Pressed, this, &AWarshipController::ActionTouchPrivate);
#endif
}

void AWarshipController::MoveForward(float Value, EInputType InputType)
{
    if (OwnedWarship)
    {
        if (EInputType::Keyboard == InputType)
        {
            OwnedWarship->MoveForward(Value);
        }
        else if (EInputType::Gamepad == InputType)
        {
            OwnedWarship->MoveForward(Value);
        }
        else if (EInputType::Touch == InputType)
        {
            OwnedWarship->MoveForward(Value);
        }
    } 
}

void AWarshipController::TurnRight(float Value, EInputType InputType)
{
    if (OwnedWarship)
    {
        if (EInputType::Keyboard == InputType)
        {
            OwnedWarship->TurnRight(Value);
        }
        else if (EInputType::Gamepad == InputType)
        {
        }
    } 
}

void AWarshipController::SetTurnAxisX(float Value)
{
    DesiredMovementVector.X = Value;
}

void AWarshipController::SetTurnAxisY(float Value)
{
    DesiredMovementVector.Y = Value;
}

void AWarshipController::TurnToVector(FVector InDesiredMovementVector)
{
    //all about rotation
    const float CurrentYaw = OwnedWarship->GetWarshipCurrentYaw();
    const float DesiredYaw = InDesiredMovementVector.Rotation().Yaw;

    const float DeltaAngleBetweenYaws = FMath::FindDeltaAngleDegrees(CurrentYaw, DesiredYaw);
    const float AbsDeltaAngleBetweenYaws = FMath::Abs(DeltaAngleBetweenYaws);
    if (AbsDeltaAngleBetweenYaws > DeltaAngleBetweenWarshipAndGamepadNotAffectedRotation)
    {
        const float DeltaAngleSigh = FMath::Sign(DeltaAngleBetweenYaws);
        OwnedWarship->TurnRight(DeltaAngleSigh);
    }
}

void AWarshipController::Action()
{
    if (OwnedWarship)
    {
        OwnedWarship->Fire();
    }
}

void AWarshipController::ActionTouchPrivate(const ETouchIndex::Type FingerIndex, const FVector Location)
{   
    UGameInstance* GameInstance = GetGameInstance();
    check(GameInstance)
    if (!GameInstance)
        return;

    UEngine* Engine = GameInstance->GetEngine();
    check(Engine)
    if (!Engine)
        return;

    UGameViewportClient* ViewportClient = Engine->GameViewport;
    check(ViewportClient)
    if (!ViewportClient)
        return;

    FViewport* Viewport = ViewportClient->Viewport;
    check(Viewport)
    if (!Viewport)
        return;

    const FIntPoint ViewportSize = Viewport->GetSizeXY();
    if (Location.X / ViewportSize.X > ActionTouchForGamePercent)
        Action();
}