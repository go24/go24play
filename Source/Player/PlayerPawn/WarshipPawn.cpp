#include "WarshipPawn.h"

#include "Engine/World.h"
#include "Classes/AIController.h"
#include <Kismet/GameplayStatics.h>
#include <ConstructorHelpers.h>
#include <Net/UnrealNetwork.h>

#include "PlayerWidget/WarshipBadge.h"
#include "Config/Public/GodConfig.h"

AWarshipPawn::AWarshipPawn(const FObjectInitializer& OI)
    : Super(OI)
{
    bReplicates = true;
    bReplicateMovement = true;
    bNetUseOwnerRelevancy = true;

    PrimaryActorTick.bCanEverTick = true;
    Mesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Warship"));
    ArrowComponent = CreateDefaultSubobject<UArrowComponent>(TEXT("Arrow"));
    MovementComponent = CreateDefaultSubobject<UWarshipPawnMovement>(TEXT("Movement"));
    WidgetComponent = CreateDefaultSubobject<UBadgeWidgetComponent>(TEXT("WarshipWidget"));

    Mesh->SetSimulatePhysics(true);
    Mesh->BodyInstance.bLockZTranslation = true;
    Mesh->BodyInstance.bLockYRotation = true;
    Mesh->SetEnableGravity(false);
    Mesh->SetAngularDamping(20.f);
    Mesh->SetLinearDamping(1.f);
    Mesh->SetMassOverrideInKg(NAME_None,5'000.f);
    Mesh->SetWorldScale3D(FVector(DefaultScale));
    RootComponent = Mesh;
    WidgetComponent->AttachToComponent(RootComponent,FAttachmentTransformRules(EAttachmentRule::KeepRelative , false));
    MovementComponent->SetUpdatedComponent(RootComponent);

    AutoPossessAI = EAutoPossessAI::PlacedInWorldOrSpawned;

    static ConstructorHelpers::FObjectFinder<UStaticMesh> WarshipMesh(TEXT("StaticMesh'/Game/Player/Assets/Pawn/SM_Player_Ship.SM_Player_Ship'"));
    if (WarshipMesh.Object)
    {
        Mesh->SetStaticMesh(WarshipMesh.Object);

        static ConstructorHelpers::FObjectFinder<UMaterialInterface>
        Shell(TEXT("Material'/Game/Player/Assets/Pawn/M_Player_ShipBody.M_Player_ShipBody'"));
        if (Shell.Object)
        {
            Mesh->SetMaterial(0, Shell.Object);

            UMaterialInterface* Material = Mesh->GetMaterial(0);
            FLinearColor LinearColor;
            FMaterialParameterInfo ParameterInfo;
            ParameterInfo.Name = "emissive_color";
            const bool IsOkay = Material->GetVectorParameterValue(ParameterInfo, LinearColor);
            if (IsOkay)
                Color = LinearColor.ToFColor(true);
        }
    }
}

void AWarshipPawn::GetLifetimeReplicatedProps(TArray<FLifetimeProperty> & OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

    DOREPLIFETIME(AWarshipPawn, MovementComponent);
}

void AWarshipPawn::BeginPlay()
{
    Super::BeginPlay();
    bIsReloaded = true;
    bIsDead = false;

    ArrowComponent->SetRelativeRotation(Mesh->GetRelativeRotation());

    // Config
    AngularSpeed = UGodConfig::Get().DesignerRow.WarshipSetup.AngularSpeed;
    CooldownTime = UGodConfig::Get().DesignerRow.WarshipSetup.CooldownTime;
    
    const float LinearDamping = UGodConfig::Get().DesignerRow.WarshipSetup.WarshipPhysicsSetup.LinearDamping;
    const float AngularDamping = UGodConfig::Get().DesignerRow.WarshipSetup.WarshipPhysicsSetup.AngularDamping;
    const float MassInKg = UGodConfig::Get().DesignerRow.WarshipSetup.WarshipPhysicsSetup.MassInKg;

    DeadScale = UGodConfig::Get().DesignerRow.WarshipSetup.DeadScale;

    Mesh->SetAngularDamping(AngularDamping);
    Mesh->SetLinearDamping(LinearDamping);
    Mesh->SetMassOverrideInKg(NAME_None, MassInKg);


    UWarshipPawnMovement* WarshipMovement = Cast<UWarshipPawnMovement>(GetMovementComponent());
    if (WarshipMovement)
    {
        WarshipMovement->MaxSpeed = UGodConfig::Get().DesignerRow.WarshipSetup.MovementSetup.MaxSpeed;
        WarshipMovement->Acceleration = UGodConfig::Get().DesignerRow.WarshipSetup.MovementSetup.Acceleration;
        WarshipMovement->Deceleration = UGodConfig::Get().DesignerRow.WarshipSetup.MovementSetup.Deceleration;
    }

    if (WidgetComponent && UserWidgetBP)
    {
        WidgetComponent->SetWidgetClass(UserWidgetBP);
        WidgetComponent->SetWidgetSpace(EWidgetSpace::Screen);

        UWarshipBadge* WarshipBadge = Cast<UWarshipBadge>(WidgetComponent->GetUserWidgetObject());
        if (WarshipBadge && WarshipBadge->CanInitialize())
        {
            WarshipBadge->Initialize();
            BadgeInstance = Cast<IWarshipBadgeInterface>(WarshipBadge);
        }
    }
}

void AWarshipPawn::Tick(float DeltaSeconds)
{
    const FVector ActorLocation = GetActorLocation();
    ArrowComponent->SetWorldLocation(ActorLocation);
    const FVector EndLine = ActorLocation + ArrowComponent->GetForwardVector() * 150;
    if (!bIsDead)
        UKismetSystemLibrary::DrawDebugArrow(GetWorld(), ActorLocation, EndLine, 50.f, Color, 0, 3);
    const float Yaw = ArrowComponent->GetForwardVector().Rotation().Yaw;;
    Mesh->SetRelativeRotation(FRotator(0, Yaw, 0));
    Super::Tick(DeltaSeconds);
}

void AWarshipPawn::MoveForward(float Accel)
{
    if (GetLocalRole() < ROLE_Authority)
    {
        Server_MoveForward(Accel);
    }
    if (bIsDead || Accel == 0.f)
        return;

    const float DeltaSeconds = UGameplayStatics::GetWorldDeltaSeconds(this);
    const FVector ForwardVector = ArrowComponent->GetForwardVector();
    const FVector Impulse = ForwardVector * Accel * MovementComponent->Acceleration * DeltaSeconds;
    if (Accel == 1.f)
    {
        AController* Controller22 = MovementComponent->GetPawnOwner()->GetController();
        if (Controller22)
        {
            bool LocController = Controller22->IsLocalController();
            bool MoveInIgn = Controller22->IsMoveInputIgnored();
            
            MovementComponent->AddInputVector(Impulse, true);
            auto Vec=  MovementComponent->GetPendingInputVector();
            MoveInIgn = false;
        }
    }
    MovementComponent->AddInputVector(Impulse, true);
    bIsLeanUpdated = true;
}

void AWarshipPawn::Server_MoveForward_Implementation(float Accel)
{
    MoveForward(Accel);
}

void AWarshipPawn::TurnRight(float Accel)
{
    if (GetLocalRole() < ROLE_Authority)
    {
        Server_TurnRight(Accel);
    }

    if (bIsDead)
        return;

    const bool IsDifferentDirection =
        Accel / FMath::Abs(Accel) != LastTurningSequence / FMath::Abs(LastTurningSequence);

    if (IsDifferentDirection || !Accel)
        LastTurningSequence = 0.f;
    
    const float DeltaSeconds = UGameplayStatics::GetWorldDeltaSeconds(this);

    if (!Accel)
    {
        const float Sign = FMath::Sign(LastTurningSequence);
        LastTurningSequence -= Sign * 5.f * DeltaSeconds;
        return;
    }
    
    const float CurrentYaw = ArrowComponent->GetForwardVector().Rotation().Yaw;
    const float RotationAngle = Accel * DeltaSeconds * AngularSpeed;
    const float Yaw = CurrentYaw + RotationAngle;
    LastTurningSequence += RotationAngle;
    RotateByYaw(Yaw);
}

void AWarshipPawn::Server_TurnRight_Implementation(float Accel)
{
    TurnRight(Accel);
}

float AWarshipPawn::GetWarshipCurrentYaw() const
{
    return ArrowComponent->GetForwardVector().Rotation().Yaw;
}

void AWarshipPawn::RotateByYaw(float Yaw)
{
    ArrowComponent->SetWorldRotation(FRotator(0.f, Yaw, 0.f));
    bIsLeanUpdated = true;
}

void AWarshipPawn::Fire()
{
    if (GetLocalRole() < ROLE_Authority)
    {
        Server_Fire();
    }

    if (bIsDead || !bIsReloaded)
        return;

    //(Algerd) ToDo: Create socket in StaticMesh to find location of where need to spawn bullet
    // TODO: Create socket in StaticMesh to find location of where need to spawn bullet (Algerd) // better than
    const float DistanceFromGun = 200.f;
    const FVector Forward = ArrowComponent->GetForwardVector();
    const FVector DirtyLocation = GetActorLocation() + Forward * DistanceFromGun;
    const FVector SpawnLocation = FVector(DirtyLocation.X, DirtyLocation.Y, 0.f);
    const FRotator SpawnRotation = Forward.Rotation();
    const FTransform SpawnTransform(SpawnRotation.Quaternion(), SpawnLocation);

    FActorSpawnParameters Parameters;
    UWorld* World = GetWorld();
    if (World)
    {
        UClass* SpawnClass = ProjectileBP.Get();
        AWarshipProjectile* Projectile = Cast<AWarshipProjectile>(UGameplayStatics::BeginDeferredActorSpawnFromClass(this, SpawnClass, SpawnTransform, ESpawnActorCollisionHandlingMethod::Undefined, this));
        if (Projectile)
        {
            Projectile->Color = Color;
            Projectile->SetColorImblaze(Color);
            UGameplayStatics::FinishSpawningActor(Projectile, SpawnTransform);
        }

        if (Projectile)
        {
                
            bIsReloaded = false;
            if (BadgeInstance)
                BadgeInstance->Execute_OnReloading(WidgetComponent->GetUserWidgetObject());
            World->GetTimerManager().SetTimer(ReloadTimerHandle, this, &AWarshipPawn::OnReloaded, CooldownTime, false);

            //event on fire
            OnFire.Broadcast();
        }
    }
}

void AWarshipPawn::Server_Fire_Implementation()
{
    Fire();
}

FColor AWarshipPawn::GetColor() const
{
    return Color;
}

void AWarshipPawn::SetColor(FColor InColor)
{
    if (GetLocalRole() == ROLE_Authority)
    {
        NetMulticast_SetColor(InColor);
    }
    Color = InColor;
    const FVector ColorVector(Color.R, Color.G, Color.B);
    Mesh->SetVectorParameterValueOnMaterials("emissive_color", ColorVector);
}

void AWarshipPawn::NetMulticast_SetColor_Implementation(FColor InColor)
{
    if (GetLocalRole() < ROLE_Authority)
    {
        SetColor(InColor);
    }
}

FVector AWarshipPawn::GetBoundsSize() const
{
    FVector WarshipSize(0.f);

    UWorld* World = GetWorld();
    if (!World)
        return WarshipSize;

    AWarshipPawn* Pawn = World->SpawnActor<AWarshipPawn>();

    FVector Origin;
    FVector BoxExtent;
    Pawn->GetActorBounds(true ,Origin, BoxExtent);
    World->DestroyActor(Pawn);

    WarshipSize = BoxExtent * 2;
    return WarshipSize;
}

float AWarshipPawn::TakeDamage(float Damage, struct FDamageEvent const& DamageEvent, AController* EventInstigator, AActor* DamageCauser)
{
    Health -= Damage;
    
    if (Health <= 0)
        Die();
    else
        OnDamage.Broadcast();

    if (BadgeInstance)
    {
        const AWarshipPawn* Enemy = Cast<AWarshipPawn>(DamageCauser);
        if (Enemy)
            BadgeInstance->Execute_PlayHit(WidgetComponent->GetUserWidgetObject(), Damage, Enemy->GetColor());

        BadgeInstance->Execute_SetHealth(WidgetComponent->GetUserWidgetObject(), Health);
    }
    return Damage;
}

void AWarshipPawn::Die()
{
    Health = 0;
    bIsDead = true;
    Mesh->SetVisibility(true);
    WidgetComponent->SetVisibility(false);
    Mesh->SetMassOverrideInKg(NAME_None, 1, true);
    
    const FVector CurrentLocation = RootComponent->GetComponentLocation();
    UPawnMovementComponent* MovementComponent = GetMovementComponent();
    MovementComponent->AddInputVector(FVector(0, 0, -100), true);
    SetActorScale3D(FVector(DeadScale));
    OnDeath.Broadcast();
}

void AWarshipPawn::Heal(const float HealthUp)
{
    Health += HealthUp;
    if (BadgeInstance)
    {
        BadgeInstance->Execute_SetHealth(WidgetComponent->GetUserWidgetObject(), Health);
        BadgeInstance->Execute_PlayHeal(WidgetComponent->GetUserWidgetObject(), Health, GetColor());
    }
}

void AWarshipPawn::Resurrect(float HealthAfter)
{
    Health = HealthAfter;
    bIsDead = false;
    if (BadgeInstance)
    {
        BadgeInstance->Execute_SetHealth(WidgetComponent->GetUserWidgetObject(), Health);
        BadgeInstance->Execute_PlayHeal(WidgetComponent->GetUserWidgetObject(), Health, GetColor());
    }
    OnRessurection.Broadcast();
    SetActorScale3D(FVector(DefaultScale));
}

void AWarshipPawn::SetId(uint8 NewId)
{
    Id = NewId;
    if (BadgeInstance)
        BadgeInstance->Execute_SetId(WidgetComponent->GetUserWidgetObject(),NewId);
}

void AWarshipPawn::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
    Super::SetupPlayerInputComponent(PlayerInputComponent);
}

void AWarshipPawn::OnReloaded()
{
    bIsReloaded = true;
    this->OnRealod.Broadcast();

    if (BadgeInstance)
        BadgeInstance->Execute_OffReloading(WidgetComponent->GetUserWidgetObject());
}