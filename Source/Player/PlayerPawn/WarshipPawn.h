#pragma once
#include <GameFramework/Pawn.h>
#include <GameFramework/PawnMovementComponent.h>
#include <Components/StaticMeshComponent.h>
#include <Components/ArrowComponent.h>
#include <Templates/SubclassOf.h>

#include "Sound/SoundCue.h"
#include "PlayerMovement/WarshipPawnMovement.h"
#include "PlayerProjectile/WarshipProjectile.h"
#include "GameInstance/Public/ProjectGameInstanceInterface.h"
#include "WidgetComponent.h"
#include "PlayerWidget/WarshipBadgeInterface.h"
#include "PlayerWidget/BadgeWidgetComponent.h"

#include "WarshipPawn.generated.h"

UCLASS()
class PLAYER_API AWarshipPawn : public APawn
{
    GENERATED_BODY()

public:
    AWarshipPawn(const FObjectInitializer& OI = FObjectInitializer::Get());

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "WarshipPawn")
    UArrowComponent* ArrowComponent;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "WarshipPawn")
    UStaticMeshComponent* Mesh;

    UPROPERTY(Replicated, EditAnywhere, BlueprintReadWrite, Category = "WarshipPawn")
    UWarshipPawnMovement* MovementComponent;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "WarshipPawn")
    UBadgeWidgetComponent* WidgetComponent;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "WarshipPawn")
    TSubclassOf<UUserWidget> UserWidgetBP;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "WarshipPawn")
    TSubclassOf<AWarshipProjectile> ProjectileBP;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "WarshipPawn")
    float AngularSpeed = 100.f;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "WarshipPawn")
    float CooldownTime = 2.f; // sec

public:
    UFUNCTION(BlueprintCallable, Category="WarshipPawn")
    void MoveForward(float Accel);

    UFUNCTION(Server, Unreliable, BlueprintCallable, Category="WarshipPawn")
    void Server_MoveForward(float Accel);

    UFUNCTION(BlueprintCallable, Category = "WarshipPawn")
    void TurnRight(float Accel);

    UFUNCTION(Server, Unreliable, BlueprintCallable, Category = "WarshipPawn")
    void Server_TurnRight(float Accel);

    UFUNCTION(BlueprintCallable, Category = "WarshipPawn")
    float GetWarshipCurrentYaw() const;

    UFUNCTION(BlueprintCallable, Category = "WarshipPawn")
    void Fire();

    UFUNCTION(Server, Unreliable, BlueprintCallable, Category = "WarshipPawn")
    void Server_Fire();

    UFUNCTION(BlueprintCallable, Category = "WarshipPawn")
    FColor GetColor() const;

    UFUNCTION(BlueprintCallable, Category = "WarshipPawn")
    void SetColor(FColor InColor);

    UFUNCTION(NetMulticast, Reliable)
    void NetMulticast_SetColor(FColor InColor);

    UFUNCTION(BlueprintCallable, Category = "WarshipPawn")
    FVector GetBoundsSize() const;

    UFUNCTION(BlueprintCallable, Category = "WarshipPawn")
    float TakeDamage(float Damage, struct FDamageEvent const& DamageEvent, AController* EventInstigator, AActor* DamageCauser) override;

    UFUNCTION(BlueprintCallable, Category = "WarshipPawn")
    void Die();

    UFUNCTION(BlueprintCallable, Category = "WarshipPawn")
    void Heal(const float HealthUp);

    UFUNCTION(BlueprintCallable, Category = "WarshipPawn")
    void Resurrect(float HealthAfter = 50.f);

    UFUNCTION(BlueprintCallable, Category = "WarshipPawn")
    uint8 GetId() const { return Id; }

    UFUNCTION(BlueprintCallable, Category = "WarshipPawn")
    void SetId(uint8 NewId = 24);

    UFUNCTION(BlueprintCallable, Category = "WarshipPawn")
    float GetHealth() const { return Health; }

    UFUNCTION(BlueprintCallable, Category = "WarshipPawn")
    bool GetIsDead() const { return bIsDead; }

protected:
    DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnFireEvent);
    UPROPERTY(BlueprintAssignable, Category = "WarshipPawn|Events")
    FOnFireEvent OnFire;

    DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnRealoded);
    UPROPERTY(BlueprintAssignable, Category = "WarshipPawn|Events")
    FOnRealoded OnRealod;

    DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnDeath);
    UPROPERTY(BlueprintAssignable, Category = "WarshipPawn|Events")
    FOnDeath OnDeath;

    DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnRessurection);
    UPROPERTY(BlueprintAssignable, Category = "WarshipPawn|Events")
    FOnRessurection OnRessurection;

    DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnDamage);
    UPROPERTY(BlueprintAssignable, Category = "WarshipPawn|Events")
    FOnDamage OnDamage;

protected:
    uint8  Id;
    FColor Color = FColor(0, 0, 1.f);

    float  Health = 100.f;

    bool bIsReloaded = true;
    bool bIsDead = false;

protected:
    virtual void BeginPlay() override;
    
    virtual void Tick(float DeltaSeconds) override;

    virtual void SetupPlayerInputComponent(UInputComponent* PlayerInputComponent) override;

    virtual void OnReloaded();

    virtual void RotateByYaw(float Yaw);

private:
    float DefaultScale = 0.2f;

    float DeadScale = 0.1f;

    FTimerHandle ReloadTimerHandle;

    float LastTurningSequence = 0.f;
    
    bool  bIsLeanUpdated;

    IWarshipBadgeInterface*        BadgeInstance = nullptr;
};