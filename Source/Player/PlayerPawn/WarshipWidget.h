#pragma once

#include <CoreMinimal.h>
#include <Blueprint/UserWidget.h>

#include "WarshipWidget.generated.h"

UCLASS()
class PLAYER_API UWarshipWidget : public UUserWidget
{
	GENERATED_BODY()
};
