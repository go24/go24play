using UnrealBuildTool;

public class Player : ModuleRules
{
    public Player(ReadOnlyTargetRules Target) : base( Target )
    {
        PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

        bEnableUndefinedIdentifierWarnings = false;

        PublicIncludePaths.AddRange(new string[] {
            "Player"
        });


        PublicDependencyModuleNames.AddRange(new string[] 
        {
            "Config",
            "GameCore",
            "GameInstance",
            "Interfaces"
        });

        PrivateDependencyModuleNames.AddRange(new string[]
        {
            "AIModule",
            "Core",
            "CoreUObject",
            "Engine",
            "UMG"
            
        });
    }
}
