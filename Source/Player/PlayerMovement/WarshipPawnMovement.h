#pragma once

#include "CoreMinimal.h"
#include "GameFramework/FloatingPawnMovement.h"
#include "WarshipPawnMovement.generated.h"

UCLASS()
class PLAYER_API UWarshipPawnMovement : public UFloatingPawnMovement
{
    GENERATED_BODY()


    UWarshipPawnMovement();
};
