#include "WarshipPawnMovement.h"

#include <GameFramework/Pawn.h>
#include <GameFramework/Controller.h>

UWarshipPawnMovement::UWarshipPawnMovement()
    : Super()
{
    MaxSpeed = 1000.f;
    Acceleration = 500.f;
    Deceleration = 200.f;
    TurningBoost = 1.0f;
    bPositionCorrected = false;
}