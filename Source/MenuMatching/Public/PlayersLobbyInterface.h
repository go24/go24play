#pragma once

#include <Interface.h>

#include "BaseTypes/Public/PlayerInfo.h"

#include "PlayersLobbyInterface.generated.h"

UINTERFACE(BlueprintType)
class MENUMATCHING_API UPlayersLobbyInterface : public UInterface
{
	GENERATED_UINTERFACE_BODY()
};

class MENUMATCHING_API IPlayersLobbyInterface
{
	GENERATED_IINTERFACE_BODY()

public:
	UFUNCTION(BlueprintImplementableEvent)
	void OnPlayerConnected(int32 InIndex, const FPlayerInfo& PlayerInfo);

	UFUNCTION(BlueprintImplementableEvent)
	void OnPlayerDisconnected(int32 InIndex);

	UFUNCTION(BlueprintImplementableEvent)
	void OnPlayerInfoChanged(int32 InIndex, const FPlayerInfo& PlayerInfo);

	UFUNCTION(BlueprintImplementableEvent)
	void OnLobbyInfoChanged(const FLobbyInfo& LobbyInfo);
};
