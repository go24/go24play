// MenuCore.Build.cs

using UnrealBuildTool;

public class MenuMatching : ModuleRules
{
    public MenuMatching(ReadOnlyTargetRules Target) : base(Target)
    {
        PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

        PublicIncludePaths.AddRange(new string[]
        {
            "MenuMatching/Public"
        });

        PrivateIncludePaths.AddRange(new string[]
        {
            "MenuMatching/Private"
        });

        PublicDependencyModuleNames.AddRange(new string[] {
            "CoreUObject",
            "Engine",
            "Core",
            "UMG",
            "Slate",
            "InputCore",
            "MenuCore"
        });

        PrivateDependencyModuleNames.AddRange(new string[]
        {
            "BaseTypes",
            "GameInstance",
            "LobbyCore",
            "Helpers"
        });
    }
}