// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include <CoreMinimal.h>
#include <Blueprint/UserWidget.h>
#include <Components/TextBlock.h>

#include "MatchingPlayerInfo.generated.h"

/**
 * 
 */
UCLASS()
class MENUMATCHING_API UMatchingPlayerInfo : public UUserWidget
{
	GENERATED_BODY()

protected:
	void NativeConstruct() override;
};
