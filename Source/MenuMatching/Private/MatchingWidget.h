#pragma once

#include <CoreMinimal.h>
#include <Blueprint/UserWidget.h>
#include <Components/ScrollBox.h>
#include <Components/ScaleBox.h>

#include "PlayersLobbyInterface.h"
#include "GameInstance/Public/ProjectGameInstanceInterface.h"

#include "MatchingWidget.generated.h"

/**
 * 
 */
UCLASS()
class MENUMATCHING_API UMatchingWidget : public UUserWidget
{
	GENERATED_BODY()
	
	UMatchingWidget(const FObjectInitializer& ObjectInitializer);

protected:
	void NativeConstruct() override;
	void NativeDestruct() override;
	
	UFUNCTION(BlueprintCallable)
	void BackToMenu();

	UFUNCTION(BlueprintCallable)
	void StartGame(FName LevelName, bool bAbsolute, FString Options);

	void NativeOnMouseEnter(const FGeometry& InGeometry, const FPointerEvent& InMouseEvent) override;
	void NativeOnMouseLeave(const FPointerEvent& InMouseEvent) override;
	FReply NativeOnKeyDown( const FGeometry& InGeometry, const FKeyEvent& InKeyEvent) override;

	void RecalculatePlayersMap(int32 RemovedPlayerId);
	void OnGamepadConnectionChanged(bool Connected, int32 PlatformUserId, int32 ControllerIndex);

	UFUNCTION(BlueprintImplementableEvent)
	void OnLobbyEmpty(bool IsEmpty);
protected:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Assets")
	TSubclassOf<class UMatchingPlayerInfo> MatchingPlayerInfoClass;

	UPROPERTY(BlueprintReadOnly, meta = (BindWidget))
	UScrollBox* PlayersListScrollBox = nullptr;

	TArray<int32> PlayerIds;
	TMap<int32, int32> PlayerIdsToListMap;

	IProjectGameInstanceInterface* ProjectGameInstance = nullptr;
};
