// Fill out your copyright notice in the Description page of Project Settings.


#include "MatchingWidget.h"

#include <UObject/ConstructorHelpers.h>
#include <Misc/CoreDelegates.h>
#include <Framework/Application/SlateApplication.h>

#include "Helpers/InputHelper/InputHelper.h"
#include "MenuPlayerController.h"
#include "MatchingPlayerInfo.h"
#include "LobbyCore/LobbyGameState.h"

UMatchingWidget::UMatchingWidget(const FObjectInitializer& ObjectInitializer)
: Super(ObjectInitializer)
{
   bIsFocusable = true;
}

void UMatchingWidget::NativeConstruct()
{
    Super::NativeConstruct();
    ProjectGameInstance = Cast<IProjectGameInstanceInterface>(GetWorld()->GetGameInstance());


    /*PlayersListScrollBox->ClearChildren();
    ProjectGameInstance = Cast<IProjectGameInstanceInterface>(GetWorld()->GetGameInstance());
    ensureMsgf(ProjectGameInstance, TEXT("Invalid Project Game Instance!"));
    if (ProjectGameInstance)
    {
        ALobbyGameState* LobbyGameState = GetWorld()->GetGameState<ALobbyGameState>();
        //ProjectGameInstance->RegisterPlayersLobbyObserver(this);
        //ProjectGameInstance->DefaultPlayerConnected();
    }

    FCoreDelegates::OnControllerConnectionChange.AddUObject(this, &UMatchingWidget::OnGamepadConnectionChanged);*/
}

void UMatchingWidget::NativeDestruct()
{
    Super::NativeDestruct();
    
    //ProjectGameInstance->UnregisterPlayersLobbyObserver(this);
}

void UMatchingWidget::BackToMenu()
{
    //ProjectGameInstance->ClearPlayersLobby();
}

void UMatchingWidget::StartGame(FName LevelName, bool bAbsolute, FString Options)
{
    //ProjectGameInstance->StartGame(LevelName, bAbsolute, Options);
}

void UMatchingWidget::NativeOnMouseEnter(const FGeometry& InGeometry, const FPointerEvent& InMouseEvent)
{
    FSlateApplication::Get().SetAllUserFocus(MyWidget.Pin());
}

void UMatchingWidget::NativeOnMouseLeave(const FPointerEvent& InMouseEvent)
{
    FSlateApplication::Get().SetAllUserFocus(MyWidget.Pin());
}

/*void UMatchingWidget::OnNewLocalPlayerRegistered(int32 PlayerId, const FPlayerInfo& PlayerInfo)
{
    if (PlayerIds.Num() == 0)
    {
        OnLobbyEmpty(false);
    }
#if WITH_EDITOR
    check(MatchingPlayerInfoClass && "Forgot to set MatchingPlayerInfoClass inside MatchingWidget");
    if (!MatchingPlayerInfoClass)
    {
        return;
    }
#endif //WITH_EDITOR

    UMatchingPlayerInfo* NewWidget = CreateWidget<UMatchingPlayerInfo>(this, MatchingPlayerInfoClass);
    FString NameStr = FString::Printf(TEXT("%s_%d"), *EnumToString(EDeviceType, PlayerInfo.DeviceType), PlayerId);
    NewWidget->SetTextName(FText::FromString(NameStr));
    PlayersListScrollBox->AddChild(NewWidget);
    PlayerIds.Add(PlayerId);
    PlayerIdsToListMap.Add(PlayerId, PlayerIds.Num() - 1);
}

void UMatchingWidget::OnLocalPlayerUnregistered(int32 PlayerId)
{
    PlayersListScrollBox->RemoveChildAt(PlayerIdsToListMap[PlayerId]);
    
    RecalculatePlayersMap(PlayerId);
}*/

void UMatchingWidget::RecalculatePlayersMap(int32 RemovedPlayerId)
{
    int32 RemovedIndex = PlayerIdsToListMap[RemovedPlayerId];
    PlayerIdsToListMap.Remove(RemovedPlayerId);
    PlayerIds.RemoveAt(RemovedIndex);
    PlayerIds.Shrink();
    for (int32 i = 0; i < PlayerIds.Num(); i++)
    {
        PlayerIdsToListMap[PlayerIds[i]] = i;
    }
    if (PlayerIds.Num() == 0)
    {
        OnLobbyEmpty(true);
    }
}

void UMatchingWidget::OnGamepadConnectionChanged(bool Connected, int32 PlatformUserId, int32 ControllerIndex)
{
    if (!Connected)
    {
        ProjectGameInstance->RequestLobbyLocalPlayerDisconnect(ControllerIndex + InputDefines::GamepadIdShift);
    }
    FSlateApplication::Get().SetAllUserFocus(MyWidget.Pin());
}

FReply UMatchingWidget::NativeOnKeyDown(const FGeometry& InGeometry, const FKeyEvent& InKeyEvent)
{
    UE_LOG(LogTemp, Warning, TEXT("OnKeyDown"));
    if (MatchingPlayerInfoClass)
    {
        uint32 ControllerId = InKeyEvent.GetUserIndex();
        FKey Key = InKeyEvent.GetKey();
        int32 NewControllerId = UInputHelper::GetControllerIdByKey(Key, ControllerId);
        if (NewControllerId != InputDefines::ControllerIdEmpty)
        {
            //TODO remove hardcode and check it from config
            if (!Key.IsGamepadKey() && (Key.GetFName() == FName("NumPadZero") || Key.GetFName() == FName("SpaceBar")))
            {
                ProjectGameInstance->RequestLobbyLocalPlayerDisconnect(NewControllerId);
            }
            else if (Key.IsGamepadKey() && Key.GetFName() == FName("Gamepad_FaceButton_Right"))
            {
                ProjectGameInstance->RequestLobbyLocalPlayerDisconnect(NewControllerId);
            }
            else
            {
                ProjectGameInstance->RequestLobbyLocalPlayerConnect(NewControllerId);
            }
        }
    }
    
    return Super::NativeOnKeyDown(InGeometry, InKeyEvent);
}