#include "ProjectGameViewportClient.h"

#include "Helpers/InputHelper/InputHelper.h"

UProjectGameViewportClient::UProjectGameViewportClient(const FObjectInitializer& ObjectInitializer)
: Super(ObjectInitializer)
{
    MaxSplitscreenPlayers = 6;
}

bool UProjectGameViewportClient::InputKey(const FInputKeyEventArgs& EventArgs)
{
    if (EventArgs.Viewport->GetClient() != this)
    {
        return false;
    }
    if (EventArgs.IsGamepad())
    {
        FInputKeyEventArgs GamepadEventArgs = EventArgs;
        GamepadEventArgs.ControllerId += InputDefines::GamepadIdShift;
        return UGameViewportClient::InputKey(GamepadEventArgs);
    }
    else
    {
       FInputKeyEventArgs KeyboardEventArgs = EventArgs;
       int32 NewControllerId = UInputHelper::GetControllerIdByKey(EventArgs.Key, EventArgs.ControllerId);
       if (NewControllerId != InputDefines::ControllerIdEmpty)
       {
           KeyboardEventArgs.ControllerId = NewControllerId;
           return UGameViewportClient::InputKey(KeyboardEventArgs);
       }
       return false;
    }
}

bool UProjectGameViewportClient::InputAxis(FViewport* InViewport, int32 ControllerId, FKey Key, float Delta, float DeltaTime, int32 NumSamples, bool bGamepad)
{
#if (PLATFORM_ANDROID || PLATFORM_IOS)
    return Super::InputAxis(InViewport, ControllerId, Key, Delta, DeltaTime, NumSamples, bGamepad);
#else
    if (bGamepad)
    {
        return Super::InputAxis(InViewport, ControllerId + InputDefines::GamepadIdShift, Key, Delta, DeltaTime, NumSamples, bGamepad);
    }
    return Super::InputAxis(InViewport, ControllerId, Key, Delta, DeltaTime, NumSamples, bGamepad);
#endif
    
}