#pragma once

#include <CoreMinimal.h>
#include <Engine/GameViewportClient.h>

#include "ProjectGameViewportClient.generated.h"

UCLASS()
class GAMEVIEWPORTCLIENT_API UProjectGameViewportClient : public UGameViewportClient
{
	GENERATED_BODY()

    UProjectGameViewportClient(const FObjectInitializer& ObjectInitializer);

public:
    bool InputKey(const FInputKeyEventArgs& EventArgs) override;
    bool InputAxis(FViewport* Viewport, int32 ControllerId, FKey Key, float Delta, float DeltaTime, int32 NumSamples=1, bool bGamepad=false) override;
};
