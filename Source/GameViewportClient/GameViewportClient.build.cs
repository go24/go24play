using UnrealBuildTool;

public class GameViewportClient : ModuleRules
{
    public GameViewportClient(ReadOnlyTargetRules Target) : base( Target )
    {
        PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

        bEnableUndefinedIdentifierWarnings = false;

        PublicIncludePaths.AddRange(new string[] 
        {
            "GameViewportClient/Public"
        });

        PrivateIncludePaths.AddRange(new string[]
        {
            "GameViewportClient/Private"
        });

        PublicDependencyModuleNames.AddRange(new string[] 
        {
            "BaseTypes",
            "Config",
            "GameInstance",
            "Helpers",
            "InputSystem",
            "MenuMatching",
            "Player"
        });

        PrivateDependencyModuleNames.AddRange(new string[]
        {
            "Core",
            "InputCore",
            "CoreUObject",
            "Engine"
        });
    }
}
