// Arena.Build.cs

using UnrealBuildTool;

public class Arena : ModuleRules
{
    public Arena(ReadOnlyTargetRules Target) : base(Target)
    {
        PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

        PublicDependencyModuleNames.AddRange(new string[] {
            "CoreUObject",
            "Engine",
            "Core",
            "GameCore"
            });
    }
}