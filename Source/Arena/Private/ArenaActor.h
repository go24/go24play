#pragma once
#include <Engine/EngineTypes.h>
#include <CoreMinimal.h>
#include <GameFramework/Actor.h>
#include <GameFramework/PlayerStart.h>

#include "ArenaCameraComponent.h"
#include "ArenaActor.generated.h"

DECLARE_MULTICAST_DELEGATE(FTestDelegate);

UCLASS()
class ARENA_API AArenaActor : public AActor
{
	GENERATED_BODY()
	
public:	
	AArenaActor();

protected:
	virtual void BeginPlay() override;

public:
	virtual void Tick(float DeltaTime) override;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "ArenaActor")
	class UArenaCameraComponent* CameraComponent;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "ArenaActor")
	class USceneComponent* SceneComponent;

	FTimerHandle ViewportInitTimer;

	void ViewportPostInit();

	void SpawnWalls(FVector2D ArenaRealSize);

    void SpawnStartPoint(const FVector2D& ArenaSize, const FVector& WarshipSize, int32 TeamQty = 2);

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FVector2D ArenaPixelSizeTest;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float ArenaAspectRatioTest;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FVector WarshipSizeTest;

	UFUNCTION(BlueprintCallable)
    virtual TArray<FVector> CalculatePositionPlayers(const FVector2D& ArenaSize, const FVector& WarshipSize) const;

	void EndPlay(EEndPlayReason::Type EndPlayReason) override;
};