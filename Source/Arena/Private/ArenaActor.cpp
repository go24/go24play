#include "ArenaActor.h"
#include "GameCore/Public/SatanGameMode.h"

#include <Engine/GameViewportClient.h>
#include <Engine.h>

AArenaActor::AArenaActor()
{
	PrimaryActorTick.bCanEverTick = true;
    SceneComponent = CreateDefaultSubobject<USceneComponent>(TEXT("SceneComponent"));

    // Make the scene component the root component
    RootComponent = SceneComponent;

    // Setup camera defaults
    CameraComponent = CreateDefaultSubobject<UArenaCameraComponent>(TEXT("CameraComponent"));
    CameraComponent->ProjectionMode = ECameraProjectionMode::Orthographic;
    CameraComponent->bConstrainAspectRatio = false;
    CameraComponent->OrthoWidth = 4000.f;
    CameraComponent->OrthoNearClipPlane = 0.f;
    CameraComponent->OrthoFarClipPlane = 10000.f;
    CameraComponent->SetupAttachment(SceneComponent);
    CameraComponent->SetRelativeLocation(FVector(0.f, 0.f, 1000.f));
    CameraComponent->SetRelativeRotation(FRotator(0.f, -90.f, 0.f));
    WarshipSizeTest = FVector(142.f, 92.f, 92.f);
}

void AArenaActor::BeginPlay()
{
	Super::BeginPlay();
    GetWorldTimerManager().SetTimer(ViewportInitTimer,this ,  &AArenaActor::ViewportPostInit, 0.1f, true, 0.f);
}

void AArenaActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void AArenaActor::SpawnWalls(FVector2D ArenaRealSize)
{

    const FVector2D BorderSize = FVector2D(ArenaRealSize.Y / 2, ArenaRealSize.X / 2);
    const float BorderThickness = 100.f;

    for (size_t i = 0; i < 4; ++i)
    {
        const bool isVertical = (i % 2 == 0);
        const bool isSecond = (i < 2);
        
        const float Multiple = isSecond ? 1 : -1;
        const float Yaw = isVertical ? 0 : 90;


        float Length = isVertical ? BorderSize.X : BorderSize.Y;
        FVector BoxExtent = (FVector(Length + BorderThickness * 2, BorderThickness, BorderThickness));
        FRotator BoxRotation = FRotator(0, Yaw, 0);
        FVector BoxLocation = FVector(isVertical
            ? FVector(0, Multiple * (BorderSize.Y + BorderThickness), 0)
            : FVector(Multiple * (BorderSize.X + BorderThickness), 0, 0)
        );

        // Spawn Settings
        UBoxComponent* BoxCollision = NewObject<UBoxComponent>(this);
        BoxCollision->RegisterComponent();
        BoxCollision->SetHiddenInGame(false);
        BoxCollision->SetWorldLocation(BoxLocation);
        BoxCollision->SetWorldRotation(BoxRotation);
        BoxCollision->SetBoxExtent(BoxExtent);
        BoxCollision->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Ignore);
        BoxCollision->SetCollisionResponseToChannel(ECollisionChannel::ECC_WorldDynamic, ECollisionResponse::ECR_Block);
        BoxCollision->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);
        BoxCollision->SetCollisionObjectType(ECollisionChannel::ECC_WorldStatic);
#if WITH_EDITOR
        BoxCollision->UpdateCollisionProfile();
#endif
        FString transformStr = BoxLocation.ToString();
        transformStr.Append(FString(TEXT("   ")));
        transformStr.Append(BoxRotation.ToString());
        transformStr.Append(FString(TEXT("   ")));
        transformStr.Append(BoxExtent.ToString());
        UE_LOG(LogTemp, Warning, TEXT("Wall transform = %s"), *transformStr);
    }
}

void AArenaActor::ViewportPostInit()
{
    UGameViewportClient* Viewport = GetWorld()->GetGameViewport();
    FVector2D ViewportPixelSize = FVector2D(0.f, 0.f);
    Viewport->GetViewportSize(ViewportPixelSize);
    UE_LOG(LogTemp, Warning, TEXT("ViewportPixelSize = %f , %f") , ViewportPixelSize.X , ViewportPixelSize.Y);
    if (ViewportPixelSize.X != 0 && ViewportPixelSize.Y != 0)
    {
        GetWorldTimerManager().ClearTimer(ViewportInitTimer);

        //Get real X and Y of Viewport
        FVector2D ViewportRealSize = FVector2D(0.f, 0.f);
        ViewportRealSize.X = CameraComponent->OrthoWidth;
        const float AspectRatio = ViewportPixelSize.X / ViewportPixelSize.Y;
        UE_LOG(LogTemp, Warning, TEXT("AspectRatio = %f"), AspectRatio);
        ViewportRealSize.Y = ViewportRealSize.X / AspectRatio;
        UE_LOG(LogTemp, Warning, TEXT("ViewportRealSize X = %f , Y = %f"), ViewportRealSize.X, ViewportRealSize.Y);

        //Debug
        ArenaPixelSizeTest = FVector2D(ViewportPixelSize.X , ViewportPixelSize.Y);
        ArenaAspectRatioTest = AspectRatio;

        // Setting Arena Area
        SpawnWalls(ViewportRealSize);
        SpawnStartPoint(ViewportRealSize, WarshipSizeTest, 2);
        ASatanGameMode* SatanGameMode = Cast<ASatanGameMode>(GetWorld()->GetAuthGameMode());
        if (SatanGameMode)
        {
            SatanGameMode->PostArenaSetup.Broadcast();
        }

    }
}

void AArenaActor::SpawnStartPoint(const FVector2D& ArenaSize, const FVector& WarshipSize, int32 TeamQty)
{
    const TArray<FVector> Points = CalculatePositionPlayers(ArenaSize, WarshipSize);

    const FRotator Rotation;
    FActorSpawnParameters Parameters;
    UWorld* World = GetWorld();
    if (World)
        for (auto& Location: Points)
        {
            World->SpawnActor<APlayerStart>(Location, Rotation, Parameters);
            /*UKismetSystemLibrary::DrawDebugSphere
            (GetWorld(), Location, 50, 12, FLinearColor(1, 1, 1, 1), 0, 10);*/
        }
}

TArray<FVector> AArenaActor::CalculatePositionPlayers(const FVector2D& ArenaSize, const FVector& WarshipSize) const
{
    TArray<FVector> ResultPointsArray;

    FIntPoint PlayersOnSideProportions = [&]()
    {
        FIntPoint Proportions;
        

        const float Ratio = ArenaSize.X / ArenaSize.Y; // Ratio 1.0 = 50% player on side
        const float OnSide = 24 / Ratio * 0.5f; 

        const int32 OnHorizontalSide = [&]
        {
            TArray<int32> Variants = { 1,2,3,4,6,8,12,24 };
            int32 Result = 24;
            float Difference = 24;
            for (int32& PlayerNum: Variants)
            {
                const float TempDifference = FMath::Abs(PlayerNum - OnSide);
                if (TempDifference < Difference)
                {
                    Result = PlayerNum;
                    Difference = TempDifference;
                }
            }
            return Result;
        }();

        Proportions.X = OnHorizontalSide;
        Proportions.Y = 24 / OnHorizontalSide;

        return Proportions;
    }();

    const float BiggestWarshipSide = FMath::Max(WarshipSize.X, WarshipSize.Y);
    const float SquareOfCenterSide = FMath::Square(BiggestWarshipSide / 2.f); // Center 
    const float RadiusFromCenter = FMath::Sqrt(SquareOfCenterSide+SquareOfCenterSide);

    const bool CanSpawnPlayerX = ArenaSize.X > BiggestWarshipSide * PlayersOnSideProportions.X;
    const bool CanSpawnPlayerY = ArenaSize.Y > BiggestWarshipSide * PlayersOnSideProportions.Y;
    if (!CanSpawnPlayerX || !CanSpawnPlayerY)
        return ResultPointsArray;

    auto OffsetFunction = [&](const float ArenaSideSize, const int32 PlayersOnSide)
    {
        const float FullFreeOffset = ArenaSideSize - (PlayersOnSide*RadiusFromCenter);
        return FullFreeOffset / PlayersOnSide;
    };

    const float OffsetX = OffsetFunction(ArenaSize.X, PlayersOnSideProportions.X);
    const float OffsetY = OffsetFunction(ArenaSize.Y, PlayersOnSideProportions.Y);

    for (int32 IndexX = 0; IndexX < PlayersOnSideProportions.X; ++IndexX)
        for (int32 IndexY = 0; IndexY < PlayersOnSideProportions.Y; ++IndexY)
        {
            const float X = IndexX * (RadiusFromCenter + OffsetX) + OffsetX / 2.f;
            const float Y = IndexY * (RadiusFromCenter + OffsetY) + OffsetY / 2.f;

            // Attension inversed Y <=> X
            const FVector LeftCornerInWorldSpace = -1 * FVector(ArenaSize.Y, ArenaSize.X, 0) / 2.f; 
            const FVector PositionOfStart = LeftCornerInWorldSpace +FVector(Y, X, 0);

            ResultPointsArray.Add(PositionOfStart);
        }

    return ResultPointsArray;
}

void AArenaActor::EndPlay(EEndPlayReason::Type EndPlayReason)
{
    Super::EndPlay(EndPlayReason);
    if (CameraComponent)
    {
       // CameraComponent->DestroyComponent();
    }
}
