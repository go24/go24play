// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Camera/CameraComponent.h"
#include "ArenaCameraComponent.generated.h"


/**
 * 
 */
UCLASS()
class ARENA_API UArenaCameraComponent : public UCameraComponent
{
    GENERATED_BODY()

public:
    UFUNCTION(BlueprintCallable)
        float GetCameraOrthoWidth() const;


	
};
