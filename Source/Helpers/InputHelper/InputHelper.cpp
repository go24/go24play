#include "InputHelper.h"

#include <Runtime/Engine/Classes/GameFramework/InputSettings.h>

const int32 InputDefines::GamepadIdShift = 2;
const int32 InputDefines::KeyboardFirstPlayerId = 0;
const int32 InputDefines::KeyboardSecondPlayerId = 1;
const int32 InputDefines::ControllerIdEmpty = -1;

int32 UInputHelper::GetControllerIdByKey(FKey Key, int32 RawControlledId)
{
    if (!Key.IsGamepadKey())
    {
        const UInputSettings* InputSettings = GetDefault<UInputSettings>();
        const TArray<FInputAxisKeyMapping>& AxisMappingsArray = InputSettings->GetAxisMappings();
        for (const FInputAxisKeyMapping& AxisMapping : AxisMappingsArray)
        {
            if (AxisMapping.Key == Key)
            {
                int32 NewControllerId = UInputHelper::BindingNameToControllerId(AxisMapping.AxisName);
                if (NewControllerId != InputDefines::ControllerIdEmpty)
                {
                    return NewControllerId;
                }
            }
        }

        const TArray<FInputActionKeyMapping>& ActionMappingsArray = InputSettings->GetActionMappings();
        for (const FInputActionKeyMapping& ActionMapping : ActionMappingsArray)
        {
            if (ActionMapping.Key == Key)
            {
                int32 NewControllerId = UInputHelper::BindingNameToControllerId(ActionMapping.ActionName);
                if (NewControllerId != InputDefines::ControllerIdEmpty)
                {
                    return NewControllerId;
                }
            }
        }

        return InputDefines::ControllerIdEmpty;
    }
    else
    {
        return RawControlledId + InputDefines::GamepadIdShift;
    }
    
    
}

int32 UInputHelper::BindingNameToControllerId(const FName& Name)
{
    FString NameString = Name.ToString();

    if (NameString.Find("1") != INDEX_NONE)
    {
        return InputDefines::KeyboardFirstPlayerId;
    }
    else if (NameString.Find("2") != INDEX_NONE)
    {
        return InputDefines::KeyboardSecondPlayerId;
    }
    return InputDefines::ControllerIdEmpty;
}