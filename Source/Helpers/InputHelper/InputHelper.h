#pragma once

#include <Kismet/BlueprintFunctionLibrary.h>

#include "InputHelper.generated.h"

UCLASS()
class HELPERS_API UInputHelper : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()

public:
	UFUNCTION(BlueprintCallable, Category="InputHelper")
	static int32 GetControllerIdByKey(FKey Key, int32 RawControlledId);

private:
	static int32 BindingNameToControllerId(const FName& Name);
};


class HELPERS_API InputDefines
{
public:
	static const int32 GamepadIdShift;
	static const int32 KeyboardFirstPlayerId;
	static const int32 KeyboardSecondPlayerId;
	static const int32 ControllerIdEmpty;
};
