using UnrealBuildTool;

public class Helpers : ModuleRules
{
    public Helpers(ReadOnlyTargetRules Target) : base(Target)
    {
        PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

        PublicIncludePaths.AddRange(new string[] {
            "Helpers"
        });

        PublicDependencyModuleNames.AddRange(new string[] {
            "Core",
            "CoreUObject",
            "BaseTypes",
            "Engine",
            "InputCore",
            "InputDevice"
        });
    }
}