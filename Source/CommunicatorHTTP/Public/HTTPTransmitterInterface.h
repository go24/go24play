#pragma once
#include <CoreMinimal.h>
#include <Interface.h>
#include "Runtime/Online/HTTP/Public/Http.h"
#include "HTTPTransmitterInterface.generated.h"

UINTERFACE(BlueprintType)
class COMMUNICATORHTTP_API UHTTPTransmitterInterface : public UInterface
{
    GENERATED_UINTERFACE_BODY()
};

class COMMUNICATORHTTP_API IHTTPTransmitterInterface
{
    GENERATED_IINTERFACE_BODY()

public:
    UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "CommunicatorHTTP")
    void Send(const FString& Json);
};