#pragma once
#include "CoreMinimal.h"
//#include "GameFramework/Actor.h"
#include "HTTPTransmitterInterface.h"
#include "Runtime/Online/HTTP/Public/Http.h"

#include "HTTPTransmitter.generated.h"

DECLARE_LOG_CATEGORY_EXTERN(HttpLog, Log, All);

UCLASS(Blueprintable, BlueprintType, ClassGroup = "UserInterface", editinlinenew, meta = (DontUseGenericSpawnObject = "True", DisableNativeTick))
class COMMUNICATORHTTP_API UHTTPTransmitter: public UObject, public IHTTPTransmitterInterface
{
    GENERATED_BODY()

public:
    UHTTPTransmitter();

    UFUNCTION(BlueprintCallable)
    void Send_Implementation(const FString& JSON);
    void Log_Amplitude_Event(const FString& event_type, const FString& cohort);

private:
    TSharedRef<IHttpRequest> GetRequest(FString Subroute);
	TSharedRef<IHttpRequest> PostRequest(FString Subroute, FString ContentJsonString);
    TSharedRef<IHttpRequest> RequestWithRoute(FString Subroute);

    void                     Send(TSharedRef<IHttpRequest>& Request);
	void                     SetRequestHeaders(TSharedRef<IHttpRequest>& Request);
	bool                     ResponseIsValid(FHttpResponsePtr Response, bool bWasSuccessful);
    void                     SetRemotePointUrl(const FString& URL);

protected:
    FString ApiBaseUrl = "http://localhost:13666";
    FString AuthorizationHeader = TEXT("Authorization");

private:
    FHttpModule* Http;
};