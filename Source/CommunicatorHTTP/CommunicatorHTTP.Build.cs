using UnrealBuildTool;

public class CommunicatorHTTP : ModuleRules
{
	public CommunicatorHTTP(ReadOnlyTargetRules Target) : base( Target )
	{
        PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

        bEnableUndefinedIdentifierWarnings = false;

        PublicIncludePaths.Add("CommunicatorHTTP");
        PublicIncludePaths.Add("CommunicatorHTTP/Public");
        PrivateIncludePaths.Add("CommunicatorHTTP/Private");
		
        PublicDependencyModuleNames.AddRange(new string[] 
        {
            "Json",
            "JsonUtilities"
        });

        PrivateDependencyModuleNames.AddRange(new string[]
        {
            "Core",
            "CoreUObject",
            "HTTP",
            "Engine"
        });
    }
}
