#include "CommunicatorHTTP/Public/HTTPTransmitter.h"
#include "Json/Public/Serialization/JsonReader.h"
#include "Json/Public/Dom/JsonObject.h"
#include "Json/Public/Serialization/JsonSerializer.h"
#include "Engine/Engine.h"
#include "GenericPlatform/GenericPlatformMisc.h"
#include "Kismet/GameplayStatics.h"

DEFINE_LOG_CATEGORY(HttpLog);

UHTTPTransmitter::UHTTPTransmitter()
{
    Http = &FHttpModule::Get();	
}

void UHTTPTransmitter::Send_Implementation(const FString& JSON)
{
    FString Subroute = "";
    TSharedRef<IHttpRequest> Post= PostRequest(Subroute, JSON);
    Send(Post);
}

void UHTTPTransmitter::Log_Amplitude_Event(const FString& event_type, const FString& cohort)
{	
	static const FString AmplitudeUrl = "https://api.amplitude.com/2/httpapi";
	static const FString ApiKey = "4161e4dc2fa878917fb4fab614c940e1";	
	static const FString UserIp = "$remote";
	static const FString Version = "0.0.1";
	static const FString Carrier = "default_carrier";

	FString OSName = UGameplayStatics::GetPlatformName();
	FString UserId = FGenericPlatformMisc::GetDeviceId();
	FString Platform = (OSName == "Windows" || OSName == "Mac" || OSName == "Linux") ? "Desktop" : "Mobile";
	UE_LOG(HttpLog, Log, TEXT("Amplitude params: %s, %s"), *OSName, *UserId);

	FString ResultJson = "{\"api_key\": \"" + ApiKey + "\", \"events\": [{\"user_id\": \"" + UserId + "\", \"event_type\": \"" +
		event_type + "\", \"user_properties\": {\"Cohort\": \"" + cohort + "\"}, \"platform\" : \"" + Platform +
		"\", \"ip\" : \"" + UserIp + "\", \"app_version\" : \"" + Version + "\", \"carrier\" : \"" + Carrier + "\", \"os_name\" : \"" + OSName + "\"}]}";

	SetRemotePointUrl(AmplitudeUrl);
	Send_Implementation(ResultJson);
}

void UHTTPTransmitter::SetRequestHeaders(TSharedRef<IHttpRequest>& Request)
{
    Request->SetHeader(TEXT("User-Agent"), TEXT("X-UnrealEngine-Agent"));
	Request->SetHeader(TEXT("Content-Type"), TEXT("application/json"));
	Request->SetHeader(TEXT("Accepts"), TEXT("application/json"));
}

void UHTTPTransmitter::SetRemotePointUrl(const FString& URL)
{
    ApiBaseUrl = URL;
}

TSharedRef<IHttpRequest> UHTTPTransmitter::GetRequest(FString Subroute)
{
    TSharedRef<IHttpRequest> Request = RequestWithRoute(Subroute);
	Request->SetVerb("GET");
	return Request;
}

TSharedRef<IHttpRequest> UHTTPTransmitter::PostRequest(FString Subroute, FString ContentJsonString)
{
    TSharedRef<IHttpRequest> Request = RequestWithRoute(Subroute);
	Request->SetVerb("POST");
	Request->SetContentAsString(ContentJsonString);
	return Request;
}

TSharedRef<IHttpRequest> UHTTPTransmitter::RequestWithRoute(FString Subroute)
{
    TSharedRef<IHttpRequest> Request = Http->CreateRequest();
	Request->SetURL(ApiBaseUrl + Subroute);
	SetRequestHeaders(Request);
	return Request;
}

void UHTTPTransmitter::Send(TSharedRef<IHttpRequest>& Request)
{
    Request->ProcessRequest();
}