// Fill out your copyright notice in the Description page of Project Settings.


#include "LobbyGameState.h"

#include <Net/UnrealNetwork.h>
#include <Kismet/GameplayStatics.h>

#include "LobbyManager.h"
#include "LobbyPlayerController.h"

void ALobbyGameState::GetLifetimeReplicatedProps(TArray<FLifetimeProperty> & OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

    DOREPLIFETIME(ALobbyGameState, LobbyManagerInstance);
}

void ALobbyGameState::PostInitializeComponents()
{
    Super::PostInitializeComponents();

    if (GetLocalRole() == ROLE_Authority)
    {
        LobbyManagerInstance = GetWorld()->SpawnActor<ALobbyManager>();
        LobbyManager = LobbyManagerInstance;
        OnLobbyManagerSpawned.Broadcast();
    }
}

void ALobbyGameState::BeginPlay()
{
    Super::BeginPlay();
}

TScriptInterface<ILobbyManagerInterface> ALobbyGameState::GetLobbyManager()
{
    return LobbyManager;
}

bool ALobbyGameState::IsLobbyManagerInitialized() const
{
    return LobbyManager != nullptr;
}

void ALobbyGameState::OnRep_LobbyManagerReplicated()
{
    bool InitialSpawn = LobbyManager == nullptr;
    LobbyManager = LobbyManagerInstance;
    if (InitialSpawn)
    {
        OnLobbyManagerSpawned.Broadcast();
    }
}