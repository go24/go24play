#include "PlayerManager.h"

#include "MenuMatching/Public/PlayersLobbyInterface.h"


UPlayerManager::UPlayerManager(const FObjectInitializer& ObjectInitializer)
: Super(ObjectInitializer)
{

}

void UPlayerManager::RegisterPlayersLobbyObserver(IPlayersLobbyInterface* InInterface)
{
    LobbyObservers.Add(InInterface);
}

void UPlayerManager::UnregisterPlayersLobbyObserver(IPlayersLobbyInterface* InInterface)
{
    int32 Index = LobbyObservers.Find(InInterface);
    if (Index != -1)
    {
        LobbyObservers.RemoveAt(Index);
    }
}

void UPlayerManager::RegisterNewPlayer_Implementation(int32 PlayerId, const FPlayerInfo PlayerInfo)
{
    FPlayerInfo* It = RegistredPlayerIdsMap.Find(PlayerId);
    if (!It)
    {
        RegistredPlayerIdsMap.Add(PlayerId, PlayerInfo);
        RegistredPlayers.Add(PlayerInfo);
        OnNewPlayerAdded(PlayerId, PlayerInfo);
    }
}

void UPlayerManager::UnregisterPlayer_Implementation(int32 PlayerId)
{
    FPlayerInfo* It = RegistredPlayerIdsMap.Find(PlayerId);
    if (It)
    {
        RegistredPlayerIdsMap.Remove(PlayerId);
        //RegistredPlayers.Remove(&It);
        OnPlayerRemoved(PlayerId);
    }
}

void UPlayerManager::ClearPlayersLobby()
{
    RegistredPlayerIdsMap.Empty();
    RegistredPlayers.Empty();
}

void UPlayerManager::OnNewPlayerAdded(int32 PlayerId, const FPlayerInfo& PlayerInfo)
{
    for (int32 i = 0; i < LobbyObservers.Num(); i++)
    {
        //LobbyObservers[i]->OnNewLocalPlayerRegistered(PlayerId, PlayerInfo);
    }
}

void UPlayerManager::OnPlayerRemoved(int32 PlayerId)
{
    for (int32 i = 0; i < LobbyObservers.Num(); i++)
    {
        //LobbyObservers[i]->OnLocalPlayerUnregistered(PlayerId);
    }
}

const TArray<FPlayerInfo>& UPlayerManager::GetRegistredPlayers() const
{
    return RegistredPlayers;
}

