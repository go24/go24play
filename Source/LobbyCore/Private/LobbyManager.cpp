#include "LobbyManager.h"

#include <Net/UnrealNetwork.h>
#include <Kismet/GameplayStatics.h>

#include "LobbyCore/LobbyGameState.h"
#include "PlayersLobbyInterface.h"
#include "TeamColor/ColorGenerator/ColorGenerator.h"

ALobbyManager::ALobbyManager(const FObjectInitializer& ObjectInitializer)
: Super(ObjectInitializer)
{
    bReplicates = true;
    bAlwaysRelevant = true;
    bNetUseOwnerRelevancy = true;
    SetReplicatingMovement(false);
    bNetLoadOnClient = true;
    SetReplicates(true);
}

void ALobbyManager::BeginPlay()
{
    Super::BeginPlay();

    ColorGenerator = GetWorld()->SpawnActor<AColorGenerator>();
    
}

void ALobbyManager::GetLifetimeReplicatedProps(TArray<FLifetimeProperty> & OutLifetimeProps) const
{
    Super::GetLifetimeReplicatedProps(OutLifetimeProps);

    DOREPLIFETIME(ALobbyManager, LobbyInfo);
    DOREPLIFETIME(ALobbyManager, LobbyPlayersInfo);
}

FColor ALobbyManager::GetColorForPlayerInfo(const FPlayerInfo& InPlayerInfo, int32 InMaxTeamNumber) const
{
    ColorGenerator->GenerateTeamColors(InMaxTeamNumber);
    return ColorGenerator->MatchColorBy(InPlayerInfo.TeamId);
}

void ALobbyManager::OnRep_LobbyInfo()
{
    NetMulticast_OnLobbyInfoChanged(LobbyInfo);
}

bool ALobbyManager::IsLeader(class APlayerController* InPlayerController) const
{
    return InPlayerController == LobbyInfo.LeadPlayerController;
}

bool ALobbyManager::CanChangeAtIndex(class APlayerController* InPlayerController, int32 InIndex) const
{
    const TArray<FPlayerInfo>& PlayerInfo = GetLobbyPlayersInfo();
    if (PlayerInfo.Num() > InIndex)
    {
        return PlayerInfo[InIndex].OwnedPlayerController == InPlayerController;
    }
    return false;
}

void ALobbyManager::RegisterLobbyObserver(TScriptInterface<IPlayersLobbyInterface> InInterface)
{
    LobbyObservers.Add(InInterface);
}

void ALobbyManager::UnregisterLobbyObserver(TScriptInterface<IPlayersLobbyInterface> InInterface)
{
    int32 Index = LobbyObservers.Find(InInterface);
    if (Index != -1)
    {
        LobbyObservers.RemoveAt(Index);
    }
}

void ALobbyManager::Server_ConnectPlayer_Implementation(const FPlayerInfo InPlayerInfo)
{
    LobbyPlayersInfo.Add(InPlayerInfo);
    NetMulticast_OnPlayerConnected(LobbyPlayersInfo.Num() - 1, InPlayerInfo);
}

void ALobbyManager::Server_DisconnectPlayer_Implementation(int32 InIndex)
{
    if (InIndex < LobbyPlayersInfo.Num())
    {
        LobbyPlayersInfo.RemoveAt(InIndex);
        NetMulticast_OnPlayerDisconnected(InIndex);
    }
}

void ALobbyManager::Server_KickPlayer_Implementation(int32 InIndex)
{
    if (InIndex < LobbyPlayersInfo.Num())
    {
        LobbyPlayersInfo.RemoveAt(InIndex);
        NetMulticast_OnPlayerDisconnected(InIndex);
    }
}

void ALobbyManager::Server_RequestChangePlayerInfo_Implementation(int32 InIndex, const FPlayerInfo InPlayerInfo)
{
    if (InIndex < LobbyPlayersInfo.Num())
    {
        LobbyPlayersInfo[InIndex] = InPlayerInfo;
        NetMulticast_OnPlayerInfoChanged(InIndex, InPlayerInfo);
    }
}

void ALobbyManager::Server_ClearPlayersLobby_Implementation()
{
    
}

void ALobbyManager::Server_SetLeadPlayerController_Implementation(class APlayerController* LeadPlayerController)
{
    LobbyInfo.LeadPlayerController = LeadPlayerController;
}

void ALobbyManager::Server_SetLobbyId_Implementation(int64 InLobbyId)
{
    LobbyInfo.LobbyId = InLobbyId;
}

void ALobbyManager::Server_SetMaxTeamNumber_Implementation(int32 InMaxTeamNumber)
{
    if (InMaxTeamNumber < LobbyInfo.MaxTeamNumber)
    {
        for (int32 i = 0; i < LobbyPlayersInfo.Num(); i++)
        {
            if (LobbyPlayersInfo[i].TeamId > InMaxTeamNumber - 1)
            {
                LobbyPlayersInfo[i].TeamId = InMaxTeamNumber - 1;
                Server_RequestChangePlayerInfo(i, LobbyPlayersInfo[i]);
            }
        }
    }
    LobbyInfo.MaxTeamNumber = InMaxTeamNumber;
    NetMulticast_OnLobbyInfoChanged(LobbyInfo);
}

void ALobbyManager::Server_SetGameType_Implementation(EGameType InGameType)
{
    LobbyInfo.GameType = InGameType;
    NetMulticast_OnLobbyInfoChanged(LobbyInfo);
}

const FLobbyInfo& ALobbyManager::GetLobbyInfo() const
{
    return LobbyInfo;
}

const TArray<FPlayerInfo>& ALobbyManager::GetLobbyPlayersInfo() const
{
    return LobbyPlayersInfo;
}

void ALobbyManager::NetMulticast_OnPlayerConnected_Implementation(int32 InIndex, const FPlayerInfo& InPlayerInfo)
{
    for (int32 i = 0; i < LobbyObservers.Num(); i++)
    {
        LobbyObservers[i]->Execute_OnPlayerConnected(LobbyObservers[i].GetObject(), InIndex, InPlayerInfo);
    }
}

void ALobbyManager::NetMulticast_OnPlayerDisconnected_Implementation(int32 InIndex)
{
    for (int32 i = 0; i < LobbyObservers.Num(); i++)
    {
        LobbyObservers[i]->Execute_OnPlayerDisconnected(LobbyObservers[i].GetObject(), InIndex);
    }
}

void ALobbyManager::NetMulticast_OnPlayerInfoChanged_Implementation(int32 InIndex, const FPlayerInfo& InPlayerInfo)
{
    for (int32 i = 0; i < LobbyObservers.Num(); i++)
    {
        LobbyObservers[i]->Execute_OnPlayerInfoChanged(LobbyObservers[i].GetObject(), InIndex, InPlayerInfo);
    }
}

void ALobbyManager::NetMulticast_OnLobbyInfoChanged_Implementation(const FLobbyInfo& InLobbyInfo)
{
    for (int32 i = 0; i < LobbyObservers.Num(); i++)
    {
        LobbyObservers[i]->Execute_OnLobbyInfoChanged(LobbyObservers[i].GetObject(), InLobbyInfo);
    }
}




/*#include "PlayerManager.h"

#include "MenuMatching/Public/PlayersLobbyInterface.h"


UPlayerManager::UPlayerManager(const FObjectInitializer& ObjectInitializer)
: Super(ObjectInitializer)
{

}

void UPlayerManager::RegisterPlayersLobbyObserver(IPlayersLobbyInterface* InInterface)
{
    LobbyObservers.Add(InInterface);
}

void UPlayerManager::UnregisterPlayersLobbyObserver(IPlayersLobbyInterface* InInterface)
{
    int32 Index = LobbyObservers.Find(InInterface);
    if (Index != -1)
    {
        LobbyObservers.RemoveAt(Index);
    }
}

void UPlayerManager::RegisterNewPlayer(int32 PlayerId, const FPlayerInfo PlayerInfo)
{
    FPlayerInfo* It = RegistredPlayerIdsMap.Find(PlayerId);
    if (!It)
    {
        RegistredPlayerIdsMap.Add(PlayerId, PlayerInfo);
        RegistredPlayers.Add(PlayerInfo);
        OnNewPlayerAdded(PlayerId, PlayerInfo);
    }
}

void UPlayerManager::UnregisterPlayer(int32 PlayerId)
{
    FPlayerInfo* It = RegistredPlayerIdsMap.Find(PlayerId);
    if (It)
    {
        RegistredPlayerIdsMap.Remove(PlayerId);
        //RegistredPlayers.Remove(&It);
        OnPlayerRemoved(PlayerId);
    }
}

void UPlayerManager::ClearPlayersLobby()
{
    RegistredPlayerIdsMap.Empty();
    RegistredPlayers.Empty();
}

void UPlayerManager::OnNewPlayerAdded(int32 PlayerId, const FPlayerInfo& PlayerInfo)
{
    for (int32 i = 0; i < LobbyObservers.Num(); i++)
    {
        LobbyObservers[i]->OnNewLocalPlayerRegistered(PlayerId, PlayerInfo);
    }
}

void UPlayerManager::OnPlayerRemoved(int32 PlayerId)
{
    for (int32 i = 0; i < LobbyObservers.Num(); i++)
    {
        LobbyObservers[i]->OnLocalPlayerUnregistered(PlayerId);
    }
}

const TArray<FPlayerInfo>& UPlayerManager::GetRegistredPlayers() const
{
    return RegistredPlayers;
}


void UProjectGameInstance::RegisterPlayersLobbyObserver(IPlayersLobbyInterface* InInterface)
{
    PlayerManager->RegisterPlayersLobbyObserver(InInterface);
}

void UProjectGameInstance::UnregisterPlayersLobbyObserver(IPlayersLobbyInterface* InInterface)
{
    PlayerManager->UnregisterPlayersLobbyObserver(InInterface);
}
















*/

