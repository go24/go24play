#pragma once

#include <CoreMinimal.h>
#include <UObject/UObjectGlobals.h>
#include <GameFramework/Info.h>

#include "LobbyManagerRequesterInterface.h"
#include "LobbyCore/LobbyGameState.h"

#include "LobbyManagerRequester.generated.h"

class IPlayersLobbyInterface;

UCLASS()
class LOBBYCORE_API ALobbyManagerRequester : public AInfo, public ILobbyManagerRequesterInterface
{
    GENERATED_BODY()

public:
    ALobbyManagerRequester(const FObjectInitializer& ObjectInitializer);

    void SetOwner(AActor* NewOwner) override;

    UFUNCTION(BlueprintCallable)
    bool IsLeader() const override;
    UFUNCTION(BlueprintCallable)
    bool CanChangeAtIndex(int32 InIndex) const override;

    UFUNCTION(Server, Reliable, BlueprintCallable)
    void Server_StartGame(const FString& InLevelName) override;
    UFUNCTION(Server, Reliable, BlueprintCallable)
    void Server_KickPlayer(int32 InIndex) override;
    UFUNCTION(Server, Reliable, BlueprintCallable)
    void Server_RequestChangePlayerInfo(int32 InIndex, const FPlayerInfo InPlayerInfo) override;

    //lobby commands
    UFUNCTION(Server, Reliable, BlueprintCallable)
    void Server_SetMaxTeamNumber(int32 InMaxTeamNumber) override;
    UFUNCTION(Server, Reliable, BlueprintCallable)
    void Server_SetGameType(EGameType InGameType) override;

    UFUNCTION(BlueprintCallable)
    const FLobbyInfo& GetLobbyInfo() const override;
    UFUNCTION(BlueprintCallable)
    const TArray<FPlayerInfo>& GetLobbyPlayersInfo() const override;
protected:
    void BeginPlay() override;

    UFUNCTION()
    void OnLobbyManagerSpawned();

private:
    UPROPERTY()
    TScriptInterface<ILobbyManagerInterface> LobbyManager;

    UPROPERTY()
    class APlayerController* OwnedPlayerController;

};