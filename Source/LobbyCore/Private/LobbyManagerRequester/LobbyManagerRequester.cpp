#include "LobbyManagerRequester.h"

#include <Net/UnrealNetwork.h>
#include <Kismet/GameplayStatics.h>
#include <Engine/World.h>

#include "LobbyCore/LobbyGameState.h"
#include "PlayersLobbyInterface.h"
#include "ProjectGameInstance.h"

ALobbyManagerRequester::ALobbyManagerRequester(const FObjectInitializer& ObjectInitializer)
: Super(ObjectInitializer)
{
    SetReplicates(true);
}

void ALobbyManagerRequester::BeginPlay()
{
    Super::BeginPlay();

    //local fields set (all sides = server + client)
    ALobbyGameState* GameState = GetWorld()->GetGameState<ALobbyGameState>();
    LobbyManager = GameState->GetLobbyManager();
    if (!LobbyManager)
    {
        GameState->OnLobbyManagerSpawned.AddDynamic(this, &ALobbyManagerRequester::OnLobbyManagerSpawned);
    }
}

void ALobbyManagerRequester::OnLobbyManagerSpawned()
{
    ALobbyGameState* GameState = GetWorld()->GetGameState<ALobbyGameState>();
    LobbyManager = GameState->GetLobbyManager();
    GameState->OnLobbyManagerSpawned.RemoveDynamic(this, &ALobbyManagerRequester::OnLobbyManagerSpawned);
}

void ALobbyManagerRequester::SetOwner(AActor* NewOwner)
{
    Super::SetOwner(NewOwner);
    OwnedPlayerController = Cast<APlayerController>(NewOwner);
}

bool ALobbyManagerRequester::IsLeader() const
{
    return LobbyManager && LobbyManager->IsLeader(OwnedPlayerController);
    
}

bool ALobbyManagerRequester::CanChangeAtIndex(int32 InIndex) const
{
    return LobbyManager && LobbyManager->CanChangeAtIndex(OwnedPlayerController, InIndex);
}

void ALobbyManagerRequester::Server_StartGame_Implementation(const FString& InLevelName)
{
    if (IsLeader())
    {
        //save current lobby setup into game instance
        UProjectGameInstance* ProjectGameInstance = Cast<UProjectGameInstance>(GetWorld()->GetGameInstance());
        if (ProjectGameInstance)
        {
            ProjectGameInstance->TransferLobbyInfo(LobbyManager->GetLobbyInfo(), LobbyManager->GetLobbyPlayersInfo());
        }
        //SEAMLESS TRAVEL TRY
        /*FString NewString = InLevelName;
        NewString.Append("?listen");
        GetWorld()->ServerTravel(NewString, true);*/

        FString NewString = FString("servertravel ");
        NewString.Append(InLevelName);
        
        UGameplayStatics::GetPlayerController(this, 0)->ConsoleCommand(NewString);
    }
}

void ALobbyManagerRequester::Server_KickPlayer_Implementation(int32 InIndex)
{
    if (IsLeader())
    {
        LobbyManager->Server_KickPlayer(InIndex);
    }
}

void ALobbyManagerRequester::Server_RequestChangePlayerInfo_Implementation(int32 InIndex, const FPlayerInfo InPlayerInfo)
{
    if (CanChangeAtIndex(InIndex))
    {
        LobbyManager->Server_RequestChangePlayerInfo(InIndex, InPlayerInfo);
    }
}

void ALobbyManagerRequester::Server_SetMaxTeamNumber_Implementation(int32 InMaxTeamNumber)
{
    if (IsLeader())
    {
        LobbyManager->Server_SetMaxTeamNumber(InMaxTeamNumber);
    }
}

void ALobbyManagerRequester::Server_SetGameType_Implementation(EGameType InGameType)
{
    if (IsLeader())
    {
        LobbyManager->Server_SetGameType(InGameType);
    }
}

const FLobbyInfo& ALobbyManagerRequester::GetLobbyInfo() const
{
    return LobbyManager->GetLobbyInfo();
}

const TArray<FPlayerInfo>& ALobbyManagerRequester::GetLobbyPlayersInfo() const
{
    return LobbyManager->GetLobbyPlayersInfo();
}

