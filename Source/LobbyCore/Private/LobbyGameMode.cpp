// Fill out your copyright notice in the Description page of Project Settings.


#include "LobbyGameMode.h"

#include <Engine/GameInstance.h>
#include <Net/UnrealNetwork.h>
#include <Kismet/GameplayStatics.h>
#include <Engine/World.h>

#include "LobbyManager.h"
#include "LobbyGameState.h"
#include "LobbyPlayerController.h"

void ALobbyGameMode::SetPlayerInfo(ALobbyPlayerController* OwnedController, FPlayerInfo InPlayerInfo)
{
    ALobbyGameState* LobbyGameState = GetGameState<ALobbyGameState>();
    if (LobbyGameState)
    {
        //FPlayerInfo& PlayerInfo = LobbyGameState->LobbyPlayersInfo[ConnectedPlayersToLobbyPlayeInfoIndex[OwnedController]];
        //PlayerInfo.DeviceType = InPlayerInfo.DeviceType;
    }
    RefreshPlayersList();
}

void ALobbyGameMode::PreLogin(const FString& Options, const FString& Address, const FUniqueNetIdRepl& UniqueId, FString& ErrorMessage)
{
    
    Super::PreLogin(Options, Address, UniqueId, ErrorMessage);
}

void ALobbyGameMode::PostLogin(APlayerController* NewPlayer)
{
    //bUseSeamlessTravel = true;
    Super::PostLogin(NewPlayer);

    ALobbyPlayerController* JoiningPlayer = Cast<ALobbyPlayerController>(NewPlayer);

	if (JoiningPlayer)
    {
        uint8 PlayerIndex = JoiningPlayer->NetPlayerIndex;

        if (PlayerIndex == 0)
        {
            JoiningPlayer->Server_SpawnLobbyManagerRequester();
            ActiveLobbyRequesterPlayerControllers.Add(JoiningPlayer);
            ClientUniqueId++;
        }
        JoiningPlayer->Client_FillUniqueClientID(ClientUniqueId);

        int32 UniqueId = JoiningPlayer->GetUniqueID();
        
        ConnectedPlayers.Add(JoiningPlayer);

        int32 Index = JoiningPlayer->GetLinkerIndex();
        int32 IDS = UGameplayStatics::GetPlayerControllerID(JoiningPlayer);

        FPlayerInfo PlayerInfo;
        PlayerInfo.OwnedPlayerController = JoiningPlayer;
        PlayerInfo.UniquePlayerId = ClientUniqueId;
        PlayerInfo.PlayerId = PlayerIndex;
        PlayerInfo.TeamId = 0;

        ALobbyGameState* LobbyGameState = GetGameState<ALobbyGameState>();
        if (LobbyGameState)
        {
            TScriptInterface<ILobbyManagerInterface> LobbyManager = LobbyGameState->GetLobbyManager();
            if (ConnectedPlayersToLobbyPlayeInfoIndex.Num() == 0)
            {
                LobbyManager->Server_SetLeadPlayerController(JoiningPlayer);
            }
            LobbyManager->Server_ConnectPlayer(PlayerInfo);
        }

        ConnectedPlayersToLobbyPlayeInfoIndex.Add(JoiningPlayer, LobbyGameState->GetLobbyManager()->GetLobbyPlayersInfo().Num() - 1);
        //JoiningPlayer->Client_FillPlayerInfo(PlayerIndex);
    }

    RefreshPlayersList();
}

void ALobbyGameMode::Logout(AController* Exiting)
{
    Super::Logout(Exiting);

    ALobbyPlayerController* ExitingPlayer = Cast<ALobbyPlayerController>(Exiting);

    if (ExitingPlayer)
    {
        
        {
            //LobbyGameState->GetLobbyManager()->DisconnectPlayer(ConnectedPlayersToLobbyPlayeInfoIndex[ExitingPlayer]);
            ConnectedPlayersToLobbyPlayeInfoIndex.Remove(ExitingPlayer);
        }

        ConnectedPlayers.Remove(ExitingPlayer);
    }

    RefreshPlayersList();
}

void ALobbyGameMode::RefreshPlayersList()
{
    ALobbyGameState* LobbyGameState = GetGameState<ALobbyGameState>();
    if (LobbyGameState)
    {

        for (ALobbyPlayerController* Player : ConnectedPlayers)
        {
            //Player->Client_RefreshLobbyList(LobbyGameState->GetLobbyManager()->LobbyPlayersInfo);
        }
    }
}

void ALobbyGameMode::BeginPlay()
{
    Super::BeginPlay();

    //PlayerManager = NewObject<UPlayerManager>(this);
}

void ALobbyGameMode::UpdateLobbyManagers(TScriptInterface<ILobbyManagerInterface> InLobbyManager)
{
    
}