// Fill out your copyright notice in the Description page of Project Settings.


#include "LobbyPlayerController.h"

#include <Kismet/GameplayStatics.h>
#include <Engine/World.h>
#include <Net/UnrealNetwork.h>

#include "LobbyGameMode.h"
#include "ProjectGameInstance.h"

ALobbyPlayerController::ALobbyPlayerController()
{
}

void ALobbyPlayerController::PostInitializeComponents()
{
    Super::PostInitializeComponents();

    /*if (NetPlayerIndex == 0)
    {
        if (GetLocalRole() == ROLE_Authority)
        {
            Server_SpawnLobbyManagerRequester();
        }
    }*/
}

void ALobbyPlayerController::Client_FillUniqueClientID_Implementation(int32 InUniqueClientID)
{
    UProjectGameInstance* ProjectGameInstance = Cast<UProjectGameInstance>(GetWorld()->GetGameInstance());
    if (ProjectGameInstance)
    {
        ProjectGameInstance->SetUniqueClientID(InUniqueClientID);
    }
}

void ALobbyPlayerController::SeamlessTravelTo(class APlayerController* NewPC)
{
    UE_LOG(LogGameMode, Warning, TEXT("FUCK SEAMLESS TRAVEL FOR PlayerController for %s"), *NewPC->GetHumanReadableName());
    AGameModeBase* mode = Cast<AGameModeBase>(GetWorld()->GetAuthGameMode());

    Super::SeamlessTravelTo(NewPC);
}

void ALobbyPlayerController::SeamlessTravelFrom(class APlayerController* OldPC)
{
    AGameModeBase* mode = Cast<AGameModeBase>(GetWorld()->GetAuthGameMode());

    Super::SeamlessTravelFrom(OldPC);
}

void ALobbyPlayerController::GetLifetimeReplicatedProps(TArray<FLifetimeProperty> & OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

    DOREPLIFETIME(ALobbyPlayerController, LobbyManagerRequesterInstance);
}

void ALobbyPlayerController::BeginPlay()
{
    Super::BeginPlay();
}

TScriptInterface<ILobbyManagerRequesterInterface> ALobbyPlayerController::GetLobbyManagerRequester() const
{
    return LobbyManagerRequester;
}

bool ALobbyPlayerController::IsLobbyManagerRequesterInitialized() const
{
    return LobbyManagerRequester != nullptr;
}

void ALobbyPlayerController::Server_SpawnLobbyManagerRequester_Implementation()
{
    LobbyManagerRequesterInstance = GetWorld()->SpawnActor<ALobbyManagerRequester>();
    LobbyManagerRequesterInstance->SetOwner(this);
    
    //LobbyManagerRequesterInstance->Client_SetOwnedPlayerController(this);
    if (GetLocalRole() == ROLE_Authority)
    {
        //LobbyManagerRequesterInstance->SetOwnedPlayerController(this);
        LobbyManagerRequester = LobbyManagerRequesterInstance;
    }
}

void ALobbyPlayerController::OnRep_LobbyManagerRequesterReplicated()
{
    bool InitialSpawn = LobbyManagerRequester == nullptr;
    LobbyManagerRequester = LobbyManagerRequesterInstance;
    if (InitialSpawn)
    {
        OnLobbyRequesterSpawned.Broadcast();
    }
}