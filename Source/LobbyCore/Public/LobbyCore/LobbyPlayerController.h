// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include <CoreMinimal.h>
#include <GameFramework/PlayerController.h>

#include "PlayerInfo.h"
#include "LobbyManagerInterface.h"
#include "LobbyManagerRequester/LobbyManagerRequester.h"

#include "LobbyPlayerController.generated.h"


DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnLobbyRequesterSpawned);
/**
 * 
 */
UCLASS()
class LOBBYCORE_API ALobbyPlayerController : public APlayerController
{
	GENERATED_BODY()

	ALobbyPlayerController();

public:
	void PostInitializeComponents() override;

	UFUNCTION(BlueprintCallable)
	TScriptInterface<ILobbyManagerRequesterInterface> GetLobbyManagerRequester() const;

	UFUNCTION(BlueprintCallable)
	bool IsLobbyManagerRequesterInitialized() const;

	UFUNCTION(Server, Reliable)
	void Server_SpawnLobbyManagerRequester();

	UFUNCTION(Client, Reliable)
	void Client_FillUniqueClientID(int32 InUniqueClientID);

	void SeamlessTravelTo(class APlayerController* NewPC) override;
	void SeamlessTravelFrom(class APlayerController* OldPC) override;

	
protected:
	void BeginPlay() override;

	UFUNCTION()
	void OnRep_LobbyManagerRequesterReplicated();

protected:
	UPROPERTY(BlueprintAssignable, Category = "Spawned Lobby Requester Event")
	FOnLobbyRequesterSpawned OnLobbyRequesterSpawned;

private:
	UPROPERTY()
	TScriptInterface<ILobbyManagerRequesterInterface> LobbyManagerRequester;

	UPROPERTY(Replicated, replicatedUsing=OnRep_LobbyManagerRequesterReplicated)
	ALobbyManagerRequester* LobbyManagerRequesterInstance;

	
};
