// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include <CoreMinimal.h>
#include <GameFramework/GameModeBase.h>

#include "LobbyManagerInterface.h"

//#include "LobbyManager.h"

#include "LobbyGameMode.generated.h"

//class ULobbyManager;


UCLASS()
class LOBBYCORE_API ALobbyGameMode : public AGameModeBase
{
	GENERATED_BODY()
	

public:
	void PreLogin(const FString& Options, const FString& Address, const FUniqueNetIdRepl& UniqueId, FString& ErrorMessage) override;

	void PostLogin(APlayerController* NewPlayer) override;

	void Logout(AController* Exiting) override;

	void SetPlayerInfo(class ALobbyPlayerController* OwnedController, struct FPlayerInfo InPlayerInfo);

	void UpdateLobbyManagers(TScriptInterface<ILobbyManagerInterface> InLobbyManager);

protected:
	void BeginPlay() override;

	void RefreshPlayersList();

protected:

	TArray<class ALobbyPlayerController*> ConnectedPlayers;
	TMap<class ALobbyPlayerController*, int32> ConnectedPlayersToLobbyPlayeInfoIndex;
	
private:
	UPROPERTY()
	TArray<class ALobbyPlayerController*> ActiveLobbyRequesterPlayerControllers;

	int32 ClientUniqueId = 0;

	//UPROPERTY(Replicated)
	//UPROPERTY()
	//ULobbyManager* LobbyManager = nullptr;
};
