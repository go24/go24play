// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include <CoreMinimal.h>
#include <GameFramework/GameStateBase.h>

#include "PlayerInfo.h"

#include "LobbyManagerInterface.h"
#include "LobbyManager.h"

#include "LobbyGameState.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnLobbyManagerSpawned);
/**
 * 
 */
UCLASS()
class LOBBYCORE_API ALobbyGameState : public AGameStateBase
{
	GENERATED_BODY()
	
public:
	void PostInitializeComponents() override;

	UFUNCTION(BlueprintCallable)
	TScriptInterface<ILobbyManagerInterface> GetLobbyManager();

	UFUNCTION(BlueprintCallable)
	bool IsLobbyManagerInitialized() const;

protected:
	void BeginPlay() override;

	UFUNCTION()
	void OnRep_LobbyManagerReplicated();

public:
	UPROPERTY(BlueprintAssignable, Category = "Spawned Lobby Manager Event")
	FOnLobbyManagerSpawned OnLobbyManagerSpawned;

private:
	UPROPERTY()
	TScriptInterface<ILobbyManagerInterface> LobbyManager;

	UPROPERTY(Replicated, replicatedUsing=OnRep_LobbyManagerReplicated)
	ALobbyManager* LobbyManagerInstance;
};
