#pragma once

#include <CoreMinimal.h>
#include <UObject/UObjectGlobals.h>

#include "PlayerManagerInterface.h"

#include "PlayerManager.generated.h"

class IPlayersLobbyInterface;

UCLASS()
class LOBBYCORE_API UPlayerManager : public UObject, public IPlayerManagerInterface
{
    GENERATED_BODY()

public:
    UPlayerManager(const FObjectInitializer& ObjectInitializer);

    void RegisterPlayersLobbyObserver(IPlayersLobbyInterface* InInterface) override;
    void UnregisterPlayersLobbyObserver(IPlayersLobbyInterface* InInterface) override;

    UFUNCTION(Server, Reliable)
    void RegisterNewPlayer(int32 PlayerId, const FPlayerInfo PlayerInfo) override;
    UFUNCTION(Server, Reliable)
    void UnregisterPlayer(int32 PlayerId) override;
    void ClearPlayersLobby() override;

    const TArray<FPlayerInfo>& GetRegistredPlayers() const override;

protected:
    void OnNewPlayerAdded(int32 PlayerId, const FPlayerInfo& PlayerInfo);
    void OnPlayerRemoved(int32 PlayerId);

private:
    TArray<FPlayerInfo> RegistredPlayers;
    TMap<int32, FPlayerInfo> RegistredPlayerIdsMap;

    TArray<IPlayersLobbyInterface*> LobbyObservers;
};
