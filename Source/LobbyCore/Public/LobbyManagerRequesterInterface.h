#pragma once

#include <Interface.h>
#include "BaseTypes/Public/PlayerInfo.h"

#include "LobbyManagerRequesterInterface.generated.h"

class IPlayersLobbyInterface;

UINTERFACE(meta = (CannotImplementInterfaceInBlueprint))
class LOBBYCORE_API ULobbyManagerRequesterInterface : public UInterface
{
    GENERATED_UINTERFACE_BODY()
};

class LOBBYCORE_API ILobbyManagerRequesterInterface
{
    GENERATED_IINTERFACE_BODY()

public:
    UFUNCTION(BlueprintCallable)
    virtual bool IsLeader() const = 0;
    UFUNCTION(BlueprintCallable)
    virtual bool CanChangeAtIndex(int32 InIndex) const = 0;

    UFUNCTION(Server, Reliable, BlueprintCallable)
    virtual void Server_StartGame(const FString& InLevelName) = 0;

    UFUNCTION(Server, Reliable, BlueprintCallable)
    virtual void Server_KickPlayer(int32 InIndex) = 0;
    UFUNCTION(Server, Reliable, BlueprintCallable)
    virtual void Server_RequestChangePlayerInfo(int32 InIndex, const FPlayerInfo InPlayerInfo) = 0;

    //lobby commands
    UFUNCTION(Server, Reliable, BlueprintCallable)
    virtual void Server_SetMaxTeamNumber(int32 InMaxTeamNumber) = 0;
    UFUNCTION(Server, Reliable, BlueprintCallable)
    virtual void Server_SetGameType(EGameType InGameType) = 0;

    UFUNCTION(BlueprintCallable)
    virtual const FLobbyInfo& GetLobbyInfo() const = 0;
    UFUNCTION(BlueprintCallable)
    virtual const TArray<FPlayerInfo>& GetLobbyPlayersInfo() const = 0;
};
