#pragma once

#include <Interface.h>
#include "BaseTypes/Public/PlayerInfo.h"

#include "PlayerManagerInterface.generated.h"

class IPlayersLobbyInterface;

UINTERFACE(BlueprintType)
class LOBBYCORE_API UPlayerManagerInterface : public UInterface
{
	GENERATED_UINTERFACE_BODY()
};

class LOBBYCORE_API IPlayerManagerInterface
{
	GENERATED_IINTERFACE_BODY()

public:
	virtual void RegisterPlayersLobbyObserver(IPlayersLobbyInterface* InInterface) = 0;
	virtual void UnregisterPlayersLobbyObserver(IPlayersLobbyInterface* InInterface) = 0;

	//UFUNCTION(NetMulticast, Reliable)
	virtual void RegisterNewPlayer(int32 PlayerId, const FPlayerInfo PlayerInfo) = 0;
	//UFUNCTION(NetMulticast, Reliable)
	virtual void UnregisterPlayer(int32 PlayerId) = 0;

	virtual void ClearPlayersLobby() = 0;
	
	virtual const TArray<FPlayerInfo>& GetRegistredPlayers() const = 0;
};
