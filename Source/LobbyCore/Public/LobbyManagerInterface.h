#pragma once

#include <Interface.h>
#include "BaseTypes/Public/PlayerInfo.h"

#include "LobbyManagerInterface.generated.h"

class IPlayersLobbyInterface;

UINTERFACE(meta = (CannotImplementInterfaceInBlueprint))
class LOBBYCORE_API ULobbyManagerInterface : public UInterface
{
    GENERATED_UINTERFACE_BODY()
};

class LOBBYCORE_API ILobbyManagerInterface
{
    GENERATED_IINTERFACE_BODY()

public:
    UFUNCTION(BlueprintCallable)
    virtual bool IsLeader(class APlayerController* InPlayerController) const = 0;
    UFUNCTION(BlueprintCallable)
    virtual bool CanChangeAtIndex(class APlayerController* InPlayerController, int32 InIndex) const = 0;
    UFUNCTION(BlueprintCallable)
    virtual FColor GetColorForPlayerInfo(const FPlayerInfo& InPlayerInfo, int32 InMaxTeamNumber) const = 0;

    UFUNCTION(BlueprintCallable)
    virtual void RegisterLobbyObserver(TScriptInterface<IPlayersLobbyInterface> InInterface) = 0;
    UFUNCTION(BlueprintCallable)
    virtual void UnregisterLobbyObserver(TScriptInterface<IPlayersLobbyInterface> InInterface) = 0;

    UFUNCTION(Server, Reliable)
    virtual void Server_ConnectPlayer(const FPlayerInfo InPlayerInfo) = 0;
    UFUNCTION(Server, Reliable)
    virtual void Server_DisconnectPlayer(int32 InIndex) = 0;
    UFUNCTION(Server, Reliable)
    virtual void Server_KickPlayer(int32 InIndex) = 0;
    UFUNCTION(Server, Reliable)
    virtual void Server_RequestChangePlayerInfo(int32 InIndex, const FPlayerInfo InPlayerInfo) = 0;
    UFUNCTION(Server, Reliable)
    virtual void Server_ClearPlayersLobby() = 0;

    //lobby commands
    UFUNCTION(Server, Reliable)
    virtual void Server_SetLeadPlayerController(class APlayerController* LeadPlayerController) = 0;
    UFUNCTION(Server, Reliable)
    virtual void Server_SetLobbyId(int64 InLobbyId) = 0;
    UFUNCTION(Server, Reliable)
    virtual void Server_SetMaxTeamNumber(int32 InMaxTeamNumber) = 0;
    UFUNCTION(Server, Reliable)
    virtual void Server_SetGameType(EGameType InGameType) = 0;

    UFUNCTION(BlueprintCallable)
    virtual const FLobbyInfo& GetLobbyInfo() const = 0;
    UFUNCTION(BlueprintCallable)
    virtual const TArray<FPlayerInfo>& GetLobbyPlayersInfo() const = 0;

    //virtual const FLobbyInfo& GetLobbyInfo() const = 0;


    //UFUNCTION(NetMulticast, Reliable)
};
