#pragma once

#include <CoreMinimal.h>
#include <UObject/UObjectGlobals.h>
#include <GameFramework/Info.h>

#include "LobbyManagerInterface.h"

#include "LobbyManager.generated.h"

class IPlayersLobbyInterface;

UCLASS()
class LOBBYCORE_API ALobbyManager : public AInfo, public ILobbyManagerInterface
{
    GENERATED_BODY()

public:
    ALobbyManager(const FObjectInitializer& ObjectInitializer);

    UFUNCTION(BlueprintCallable)
    bool IsLeader(class APlayerController* InPlayerController) const override;
    UFUNCTION(BlueprintCallable)
    bool CanChangeAtIndex(class APlayerController* InPlayerController, int32 InIndex) const override;
    UFUNCTION(BlueprintCallable)
    FColor GetColorForPlayerInfo(const FPlayerInfo& InPlayerInfo, int32 InMaxTeamNumber) const override;

    UFUNCTION(BlueprintCallable)
    void RegisterLobbyObserver(TScriptInterface<IPlayersLobbyInterface> InInterface) override;

    UFUNCTION(BlueprintCallable)
    void UnregisterLobbyObserver(TScriptInterface<IPlayersLobbyInterface> InInterface) override;

    UFUNCTION(Server, Reliable)
    void Server_ConnectPlayer(const FPlayerInfo InPlayerInfo) override;
    UFUNCTION(Server, Reliable)
    void Server_DisconnectPlayer(int32 InIndex) override;
    UFUNCTION(Server, Reliable)
    void Server_KickPlayer(int32 InIndex) override;
    UFUNCTION(Server, Reliable)
    void Server_RequestChangePlayerInfo(int32 InIndex, const FPlayerInfo InPlayerInfo) override;
    UFUNCTION(Server, Reliable)
    void Server_ClearPlayersLobby() override;
    
    //lobby commands
    UFUNCTION(Server, Reliable)
    void Server_SetLeadPlayerController(class APlayerController* LeadPlayerController) override;
    UFUNCTION(Server, Reliable)
    void Server_SetLobbyId(int64 InLobbyId) override;
    UFUNCTION(Server, Reliable)
    void Server_SetMaxTeamNumber(int32 InMaxTeamNumber) override;
    UFUNCTION(Server, Reliable)
    void Server_SetGameType(EGameType InGameType) override;

    UFUNCTION(BlueprintCallable)
    const FLobbyInfo& GetLobbyInfo() const override;
    UFUNCTION(BlueprintCallable)
    const TArray<FPlayerInfo>& GetLobbyPlayersInfo() const;

    //client functions
    UFUNCTION(NetMulticast, Reliable)
	void NetMulticast_OnPlayerConnected(int32 InIndex, const FPlayerInfo& InPlayerInfo);
    UFUNCTION(NetMulticast, Reliable)
	void NetMulticast_OnPlayerDisconnected(int32 InIndex);
    UFUNCTION(NetMulticast, Reliable)
	void NetMulticast_OnPlayerInfoChanged(int32 InIndex, const FPlayerInfo& InPlayerInfo);
    UFUNCTION(NetMulticast, Reliable)
    void NetMulticast_OnLobbyInfoChanged(const FLobbyInfo& InLobbyInfo);
protected:
    void BeginPlay() override;

protected:
    UFUNCTION()
	void OnRep_LobbyInfo();

private:
    UPROPERTY(Replicated, replicatedUsing=OnRep_LobbyInfo)
    FLobbyInfo LobbyInfo;

    UPROPERTY(Replicated)
    TArray<FPlayerInfo> LobbyPlayersInfo;

    TArray<TScriptInterface<IPlayersLobbyInterface>> LobbyObservers;

    UPROPERTY()
    class AColorGenerator* ColorGenerator = nullptr;
};

/*#include <CoreMinimal.h>


#include "PlayerManagerInterface.h"

#include "PlayerManager.generated.h"

class IPlayersLobbyInterface;

UCLASS()
class PLAYERMANAGERCORE_API UPlayerManager : public UObject, public IPlayerManagerInterface
{
    GENERATED_BODY()

public:
    UPlayerManager(const FObjectInitializer& ObjectInitializer);

    void RegisterPlayersLobbyObserver(IPlayersLobbyInterface* InInterface) override;
    void UnregisterPlayersLobbyObserver(IPlayersLobbyInterface* InInterface) override;

    //UFUNCTION(NetMulticast, Reliable)
    void RegisterNewPlayer(int32 PlayerId, const FPlayerInfo PlayerInfo) override;
    //UFUNCTION(NetMulticast, Reliable)
    void UnregisterPlayer(int32 PlayerId) override;
    void ClearPlayersLobby() override;

    const TArray<FPlayerInfo>& GetRegistredPlayers() const override;

protected:
    void OnNewPlayerAdded(int32 PlayerId, const FPlayerInfo& PlayerInfo);
    void OnPlayerRemoved(int32 PlayerId);

private:
    TArray<FPlayerInfo> RegistredPlayers;
    TMap<int32, FPlayerInfo> RegistredPlayerIdsMap;

    TArray<IPlayersLobbyInterface*> LobbyObservers;
};*/
