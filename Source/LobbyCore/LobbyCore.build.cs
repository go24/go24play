using UnrealBuildTool;

public class LobbyCore : ModuleRules
{
    public LobbyCore(ReadOnlyTargetRules Target) : base( Target )
    {
        PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

        bEnableUndefinedIdentifierWarnings = false;

        PublicIncludePaths.AddRange(new string[]
        {
            "LobbyCore/Public"
        });

        PrivateIncludePaths.AddRange(new string[]
        {
            "LobbyCore/Private"
        });


        PublicDependencyModuleNames.AddRange(new string[] 
        {
			"Player",
            "BaseTypes",
            "MenuMatching"
        });

        PrivateDependencyModuleNames.AddRange(new string[]
        {
            "Core",
            "InputCore",
            "CoreUObject",
            "Engine",
            
            "GameInstance",
            "TeamColor"
        });
    }
}
