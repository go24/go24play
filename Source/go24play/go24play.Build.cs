// Fill out your copyright notice in the Description page of Project Settings.

using UnrealBuildTool;

public class go24play : ModuleRules
{
	public go24play(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;
	
		PublicDependencyModuleNames.AddRange(new string[]
		{
			"Core",
			"CoreUObject",
			"Engine",
			"InputCore"
        });

		PrivateDependencyModuleNames.AddRange(new string[] 
		{
            "Core",
            "CoreUObject",
            "JsonUtilities",
            "Slate",
            "SlateCore",
            "Engine",
            "InputCore",
            "EditorStyle",
            "UMG"
        });
	}
}
