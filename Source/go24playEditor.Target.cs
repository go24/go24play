using UnrealBuildTool;
using System.Collections.Generic;

public class go24playEditorTarget : TargetRules
{
    public go24playEditorTarget(TargetInfo Target) : base(Target)
    {
        Type = TargetType.Editor;

        ExtraModuleNames.AddRange( new string[] 
        {
            "BaseTypes",
            "Interfaces",
            "Helpers",
            "InputSystem",
            "TeamColor",
            "Config",
            "MenuCore",
            "MenuMatching",
            "MenuStart",
            "Player",
            "AI",
            "Arena",
            "GameCore",
            "GameInstance",
            "GameViewportClient",
            "LobbyCore",
            "go24play",
            "ScreenMessage",
            "GameLiftServerSDK"
        });
    }
}
