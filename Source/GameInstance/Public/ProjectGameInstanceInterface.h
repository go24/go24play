#pragma once

#include <Interface.h>

#include "InputSystem/Public/GamepadManagerInterface.h"
#include "PlayersLobbyInterface.h"

#include "ProjectGameInstanceInterface.generated.h"

UINTERFACE(BlueprintType)
class GAMEINSTANCE_API UProjectGameInstanceInterface : public UInterface
{
	GENERATED_UINTERFACE_BODY()
};

class GAMEINSTANCE_API IProjectGameInstanceInterface
{
	GENERATED_IINTERFACE_BODY()
public:

	virtual const IGamepadManagerInterface* GetGamepadManager() const = 0;

	virtual void RequestLobbyLocalPlayerConnect(int32 LocalPlayerId) = 0;
	virtual void RequestLobbyLocalPlayerDisconnect(int32 LocalPlayerId) = 0;
};
