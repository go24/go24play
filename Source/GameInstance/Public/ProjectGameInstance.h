#pragma once

#include <CoreMinimal.h>
#include <Engine/GameInstance.h>

#include "ProjectGameInstanceInterface.h"
#include "InputSystem/Public/GamepadManagerInterface.h"
#include "Config/Public/GodConfig.h"
#include "CommunicatorHTTP/Public/HTTPTransmitter.h"

#include "ProjectGameInstance.generated.h"

class UGamepadManager;

UCLASS(Blueprintable, BlueprintType)
class GAMEINSTANCE_API UProjectGameInstance : public UGameInstance, public IProjectGameInstanceInterface
{
	GENERATED_BODY()
public:
    void Init() override;

    const IGamepadManagerInterface* GetGamepadManager() const override;

    void RequestLobbyLocalPlayerConnect(int32 LocalPlayerId) override;
	void RequestLobbyLocalPlayerDisconnect(int32 LocalPlayerId) override;

    void TransferLobbyInfo(const FLobbyInfo& InLobbyInfo, const TArray<FPlayerInfo>& InLobbyPlayersInfo);
    void SetUniqueClientID(int32 InUniqueClientID);
    int32 GetUniqueClientID() const;

    const FLobbyInfo& GetLobbyInfo() const;
    const TArray<FPlayerInfo>& GetLobbyPlayersInfo() const;

protected:
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Assets")
	TSubclassOf<UGodConfig> GodConfigClass;

private:
    UPROPERTY()
    UGamepadManager* GamepadManager = nullptr;

    UPROPERTY()
    UGodConfig* GodConfig = nullptr;

    UPROPERTY()
    UHTTPTransmitter* HttpTransmitter = nullptr;
    FLobbyInfo LobbyInfo;

    UPROPERTY()
    TArray<FPlayerInfo> LobbyPlayersInfo;

    int32 UniqueClientID = -1;
};
