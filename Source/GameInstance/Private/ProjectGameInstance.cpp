#include "ProjectGameInstance.h"

#include "PlayerManager.h"
#include "InputSystem/Public/GamepadManager.h"
#include "Config/Public/GodConfig.h"
#include "CommunicatorHTTP/Public/HTTPTransmitter.h"

#include <Kismet/GameplayStatics.h>

void UProjectGameInstance::Init()
{
    GamepadManager = NewObject<UGamepadManager>(this);
    GodConfig = NewObject<UGodConfig>(this, *GodConfigClass);

    HttpTransmitter = NewObject<UHTTPTransmitter>(this);
    HttpTransmitter->Log_Amplitude_Event("test_event_type", "Cohort_test");
}

const IGamepadManagerInterface* UProjectGameInstance::GetGamepadManager() const
{
    return GamepadManager;
}

void UProjectGameInstance::RequestLobbyLocalPlayerConnect(int32 LocalPlayerId)
{
    ULocalPlayer* LocalPlayer = FindLocalPlayerFromControllerId(LocalPlayerId);
    if (!LocalPlayer)
    {
        FString ErrorString;
        LocalPlayer = CreateLocalPlayer(LocalPlayerId, ErrorString, true);
        if (LocalPlayer)
        {
            //LocalPlayer->Get
        }
    }
}

void UProjectGameInstance::RequestLobbyLocalPlayerDisconnect(int32 LocalPlayerId)
{
    ULocalPlayer* LocalPlayer = FindLocalPlayerFromControllerId(LocalPlayerId);
    if (LocalPlayer)
    {
        RemoveLocalPlayer(LocalPlayer);
    }
}

void UProjectGameInstance::TransferLobbyInfo(const FLobbyInfo& InLobbyInfo, const TArray<FPlayerInfo>& InLobbyPlayersInfo)
{
    LobbyInfo = InLobbyInfo;
    LobbyPlayersInfo = InLobbyPlayersInfo;
}

void UProjectGameInstance::SetUniqueClientID(int32 InUniqueClientID)
{
    UniqueClientID = InUniqueClientID;
}

int32 UProjectGameInstance::GetUniqueClientID() const
{
    return UniqueClientID;
}

const FLobbyInfo& UProjectGameInstance::GetLobbyInfo() const
{
    return LobbyInfo;
}

const TArray<FPlayerInfo>& UProjectGameInstance::GetLobbyPlayersInfo() const
{
    return LobbyPlayersInfo;
}