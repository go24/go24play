using UnrealBuildTool;

public class GameInstance : ModuleRules
{
    public GameInstance(ReadOnlyTargetRules Target) : base( Target )
    {
        PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

        bEnableUndefinedIdentifierWarnings = false;

        PublicIncludePaths.AddRange(new string[] 
        {
            "GameInstance/Public"
        });

        PrivateIncludePaths.AddRange(new string[]
        {
            "GameInstance/Private"
        });

        PublicDependencyModuleNames.AddRange(new string[] 
        {
			
            "BaseTypes",
            "Config",
            "InputSystem",
            "Helpers",
            "LobbyCore",
            "MenuMatching",
            "Player",
            "CommunicatorHTTP"
        });

        PrivateDependencyModuleNames.AddRange(new string[]
        {
            "Core",
            "InputCore",
            "CoreUObject",
            "Engine"
        });
    }
}
