#pragma once

#include <CoreMinimal.h>
#include <UObject/UObjectGlobals.h>
#include "DesignerRow.h"

#include "GodConfig.generated.h"


UCLASS(Blueprintable, BlueprintType)
class CONFIG_API UGodConfig : public UObject
{
	GENERATED_BODY()

public:
    UPROPERTY(EditAnywhere, Category = "Main JSON Config")
    FDesignerRow DesignerRow;

private:
	static UGodConfig* Instance;

public:
    UGodConfig(const FObjectInitializer& ObjectInitializer);
	static const UGodConfig& Get();

#if WITH_EDITOR
    virtual void PostEditChangeChainProperty( struct FPropertyChangedChainEvent& PropertyChangedEvent );
#endif

private:
    void SaveJSONConfig();
};
