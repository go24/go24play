#pragma once
#include <limits>
#include <CoreMinimal.h>
#include <Engine/DataTable.h>

#include "DesignerRow.generated.h"

USTRUCT(BlueprintType)
struct CONFIG_API FMovementSetup
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
	float MaxSpeed = 1000.f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
	float Acceleration = 500.f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
	float Deceleration = 200.f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
	float DefaultScale = 0.2f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
	float DeadScale = 0.1f;
};

USTRUCT(BlueprintType)
struct CONFIG_API FWarshipPhysicsSetup
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Warship/Physics/Setup")
	float LinearDamping = 1.f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
	float AngularDamping = 20.f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
	float MassInKg = 5000.f;
};

USTRUCT(BlueprintType)
struct CONFIG_API FWarshipSetup
{
    GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "WarshipPawn")
	FMovementSetup MovementSetup;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "WarshipPawn")
	FWarshipPhysicsSetup WarshipPhysicsSetup;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "WarshipPawn")
	float CooldownTime = 2.f; // sec

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "WarshipPawn")
	float AngularSpeed = 100.f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "WarshipPawn")
	float DeadScale = 0.2f;

};

USTRUCT(BlueprintType)
struct CONFIG_API FProjectileSetup
{
	GENERATED_BODY()

		UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Projectile")
		float StartEnergy = 10.f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Projectile")
		float MaxEnergy = 100.f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Projectile")
		float DowngrageValue = 20.f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Projectile")
		float ImblazeTimer = 0.5f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Projectile")
		float ProjectileDieTimer = 10.f;
};

USTRUCT(BlueprintType)
struct CONFIG_API FEnergyPhysicsSetup
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Energy/Physics")
	float LinearDamping = 0.01f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Energy/Physics")
	float AngularDamping = 0.f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Energy/Physics")
	float MassInKg = 1.f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Energy/Physics")
	float Bounciness = 1.f;
};

USTRUCT(BlueprintType)
struct CONFIG_API FEnergySetup
{
    GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FProjectileSetup ProjectileSetup;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FEnergyPhysicsSetup EnergyPhysicsSetup;

    UPROPERTY(EditAnywhere, BlueprintReadWrite)
    float InitialSpeed = 100.f;

    UPROPERTY(EditAnywhere, BlueprintReadWrite)
    float MaxSpeed = 100.f;

    UPROPERTY(EditAnywhere, BlueprintReadWrite)
    float OpenTime = 1.f;
};


USTRUCT(BlueprintType)
struct CONFIG_API FTouchSetup
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "TouchConfig")
	float TouchMoveThreshold = 0.4f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "TouchConfig")
	float ActionTouchForGamePercent = 0.4f;
};


USTRUCT(BlueprintType)
struct CONFIG_API FGamepadSetup
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "GamepadConfig")
	float GamepadMoveThreshold = 0.4f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "GamepadConfig")
	float DeltaAngleBetweenWarshipAndGamepadNotAffectedRotation = 5.f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "GamepadConfig")
	float DeltaAngleBetweenWarshipAndGamepadReverse = 90.f;
};

USTRUCT(BlueprintType)
struct CONFIG_API FDesignerRow : public FTableRowBase
{
    GENERATED_BODY()

    UPROPERTY(EditAnywhere, BlueprintReadWrite)
    FWarshipSetup WarshipSetup;
    
    UPROPERTY(EditAnywhere, BlueprintReadWrite)
    FEnergySetup EnergySetup;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FGamepadSetup GamepadSetup;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FTouchSetup TouchSetup;
};