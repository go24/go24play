// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "JsonUtilities/Public/JsonObjectConverter.h"
#include "Json/Public/Dom/JsonObject.h"
#include "Json/Public/Serialization/JsonSerializer.h"
#include "Paths.h"
#include "FileHelper.h"
#include "Regex.h"
#include "HAL/PlatformFilemanager.h"
#include "Config/Public/DesignerRow.h"

DECLARE_LOG_CATEGORY_EXTERN(ConfigLog, Log, All);

class CONFIG_API UGameConfig
{
public:
	static UGameConfig& Instance() noexcept;

	TSharedPtr<FJsonObject> LoadJsonObjectFromFile(const FString& FileName);
	bool SaveJsonObjectToFile(TSharedPtr<FJsonObject> Json, const FString& FileName) const;
	void ReloadConfig();
	void RemoveConfigFile(const FString& FileName);
	
	void SetDesignerConfig(const FDesignerRow& InConfig);
	void SetDesignerConfigFromJsonObject(const TSharedPtr<FJsonObject>& Json);

	const FDesignerRow&     GetDesignerConfig() const noexcept;
	TSharedPtr<FJsonObject> GetDesignerConfigAsJsonObject() const noexcept;

    const FString& GetDefaultPath() const { return DefaultFilePath; }
    void           SetDefaultPath(const FString& InNewDefaultPath) { DefaultFilePath = InNewDefaultPath; }

protected:
    FString DefaultFilePath = "GameConfig.json";

private:
	UGameConfig();
	~UGameConfig() = default;

	void Initialize();
	void RemoveCommentsInline(FString& JsonString);
	void TrimValuesInline(FString& JsonString);

private:	
	FDesignerRow DesignerRow;
};
