#include "GameConfig.h"

DEFINE_LOG_CATEGORY(ConfigLog);

UGameConfig& UGameConfig::Instance() noexcept
{
	static UGameConfig ConfigInstance;
	return ConfigInstance;
}

TSharedPtr<FJsonObject> UGameConfig::LoadJsonObjectFromFile(const FString& FileName)
{
	const FString FilePath = FPaths::Combine(FPaths::ProjectConfigDir(), FileName);
	TSharedPtr<FJsonObject> Json = MakeShareable(new FJsonObject());

	FString JsonString;
	if (!FPaths::FileExists(FilePath))
	{
		UE_LOG(ConfigLog, Warning, TEXT("File %s not exist!"), *FileName);
		return Json;
	}

	bool IsLoad = FFileHelper::LoadFileToString(JsonString, *FilePath);
	if (!IsLoad)
	{
		UE_LOG(ConfigLog, Warning, TEXT("Can't load %s.json!"), *FileName);
		return Json;
	}

	RemoveCommentsInline(JsonString);
	TrimValuesInline(JsonString);
	
	TSharedRef<TJsonReader<TCHAR>> JsonReader = TJsonReaderFactory<TCHAR>::Create(JsonString);

	if (!FJsonSerializer::Deserialize(JsonReader, Json) || !Json.IsValid())
	{
		UE_LOG(ConfigLog, Display, TEXT("Json config error %s.json file invalid! Default values used!"), *FileName);
	}

	return Json;
}

bool UGameConfig::SaveJsonObjectToFile(TSharedPtr<FJsonObject> Json, const FString& FileName) const
{
	FString OutputString;
	TSharedRef<TJsonWriter<TCHAR>> Writer = TJsonWriterFactory<TCHAR>::Create(&OutputString);
	const FString FilePath = FPaths::Combine(FPaths::ProjectConfigDir(), FileName);

	if (!FJsonSerializer::Serialize(Json.ToSharedRef(), Writer))
	{
		UE_LOG(ConfigLog, Warning, TEXT("Could not serialize JsonObject to string while saving it to file '%s'"), *FileName);
		return false;
	}

	if (!FFileHelper::SaveStringToFile(OutputString, *FilePath))
	{
		UE_LOG(ConfigLog, Warning, TEXT("Could not save JsonObject to file '%s'"), *FileName);
		return false;
	}
	return true;
}

void UGameConfig::ReloadConfig()
{
	RemoveConfigFile(DefaultFilePath);
	auto DesignerConfigJsonObject = GetDesignerConfigAsJsonObject();
	SaveJsonObjectToFile(DesignerConfigJsonObject, DefaultFilePath);
}

void UGameConfig::RemoveConfigFile(const FString& FileName)
{
	if (FileName.IsEmpty())
	{
		return;
	}

	const FString FilePath = FPaths::Combine(FPaths::ProjectConfigDir(), FileName);
	IPlatformFile& PlatformFile = FPlatformFileManager::Get().GetPlatformFile();

	if (PlatformFile.FileExists(*FilePath))
	{
		PlatformFile.DeleteFile(*FilePath);
	}
}

void UGameConfig::SetDesignerConfig(const FDesignerRow& InConfig)
{
	DesignerRow = InConfig;
}

void UGameConfig::SetDesignerConfigFromJsonObject(const TSharedPtr<FJsonObject>& Json)
{
	if (FJsonObjectConverter::JsonObjectToUStruct<FDesignerRow>(Json.ToSharedRef(), &DesignerRow, 0, 0))
	{
	    UE_LOG(ConfigLog, Display, TEXT("Successfully converted JsonObject to UStruct"));
	}
	else
		UE_LOG(ConfigLog, Warning, TEXT("Failed to convert JsonObject to UStruct"));
}

const FDesignerRow& UGameConfig::GetDesignerConfig() const noexcept
{
	return DesignerRow;
}

TSharedPtr<FJsonObject> UGameConfig::GetDesignerConfigAsJsonObject() const noexcept
{
	return FJsonObjectConverter::UStructToJsonObject(DesignerRow, 0, 0, nullptr);
}

UGameConfig::UGameConfig()
{
	Initialize();
}

void UGameConfig::Initialize()
{
	auto JsonObject = LoadJsonObjectFromFile(DefaultFilePath);
	SetDesignerConfigFromJsonObject(JsonObject);
}

void UGameConfig::RemoveCommentsInline(FString& JsonString)
{
	const FRegexPattern Pattern{ TEXT(R"((\/\*.*\*\/)|(\/\/.*))") };
	FRegexMatcher Matcher{ Pattern, JsonString };

	FString JsonStringWithoutComments = JsonString;
	int32 DeletedSymbolsCount = 0;
	while (Matcher.FindNext())
	{
		const int32 Begin = Matcher.GetMatchBeginning() - DeletedSymbolsCount;
		const int32 End = Matcher.GetMatchEnding() - DeletedSymbolsCount;
		JsonStringWithoutComments.RemoveAt(Begin, End - Begin);
		DeletedSymbolsCount += End - Begin;
	}

	JsonString = JsonStringWithoutComments;
}

void UGameConfig::TrimValuesInline(FString& JsonString)
{
	FString JsonWithMaskedDoubleQuotation = JsonString.Replace(TEXT("\"\""), TEXT("\" \""));
	TArray<FString> Dissection;
	const int32 PartsCount = JsonWithMaskedDoubleQuotation.ParseIntoArray(Dissection, TEXT("\""));
	for (int32 i = 1; i < PartsCount; i = i + 2)
	{
		Dissection[i].TrimStartAndEndInline();
	}
	JsonString = FString::Join(Dissection, TEXT("\""));
}
