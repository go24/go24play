#include "GodConfig.h"

#include <UObject/ConstructorHelpers.h>
#include "GameConfig.h"

UGodConfig* UGodConfig::Instance = nullptr;

UGodConfig::UGodConfig(const FObjectInitializer& ObjectInitializer)
: Super(ObjectInitializer)
{
    this->DesignerRow = UGameConfig::Instance().GetDesignerConfig();
    Instance = this;
}

const UGodConfig& UGodConfig::Get()
{
    ensureMsgf(IsValid(Instance), TEXT("Invalid Config Instance!"));
    return *Instance;
}

#if WITH_EDITOR
void UGodConfig::PostEditChangeChainProperty(FPropertyChangedChainEvent& PropertyChangedEvent)
{
    SaveJSONConfig();
}
#endif

void UGodConfig::SaveJSONConfig()
{
    UGameConfig& GameConfigInstance = UGameConfig::Instance();
    GameConfigInstance.SetDesignerConfig(this->DesignerRow);
    const TSharedPtr<FJsonObject> JsonObject = GameConfigInstance.GetDesignerConfigAsJsonObject();
    const FString& ConfigPath = GameConfigInstance.GetDefaultPath();
    GameConfigInstance.SaveJsonObjectToFile(JsonObject, ConfigPath);
}
