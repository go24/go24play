// MenuCore.Build.cs

using UnrealBuildTool;

public class MenuStart : ModuleRules
{
    public MenuStart(ReadOnlyTargetRules Target) : base(Target)
    {
        PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

        //PublicIncludePaths.AddRange(
        //    new string[] {
        //        "Menu/Core/MenuController",
        //        "Menu/Core/MenuMode",
        //        "Menu/Core/MenuPawn",
        //        "Menu/Core/MenuState"
        //    });

        PublicDependencyModuleNames.AddRange(new string[] {
            "CoreUObject",
            "Engine",
            "Core",
            "UMG",
            "Slate",
            "InputCore"
        });

        PrivateDependencyModuleNames.AddRange(new string[]
        {
            "BaseTypes",
            "GameCore",
            "Interfaces",
            "Helpers"
        });
    }
}