// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include <CoreMinimal.h>
#include <Components/Button.h>

#include "StartButton.generated.h"

/**
 * 
 */
UCLASS()
class MENUSTART_API UStartButton : public UButton
{
	GENERATED_BODY()
	
};
