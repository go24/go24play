// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include <CoreMinimal.h>
#include <Blueprint/UserWidget.h>

#include "StartWidget.generated.h"

/**
 * 
 */
UCLASS()
class MENUSTART_API UStartWidget : public UUserWidget
{
	GENERATED_BODY()
public:
	UStartWidget(const FObjectInitializer& ObjectInitializer);

	// Optionally override the Blueprint "Event Construct" event
	virtual void NativeConstruct() override;

	// Optionally override the tick event
	virtual void NativeTick(const FGeometry& MyGeometry, float InDeltaTime) override;
};
