#include "GamepadManager.h"

#include <Misc/CoreDelegates.h>

#if PLATFORM_WINDOWS
#include <ApplicationCore/Private/Windows/XInputInterface.h>
#pragma pack (push,8)
#include "Windows/AllowWindowsPlatformTypes.h"
#include <xinput.h>
#include "Windows/HideWindowsPlatformTypes.h"
#pragma pack (pop)
#endif

UGamepadManager::UGamepadManager(const FObjectInitializer& ObjectInitializer)
: Super(ObjectInitializer)
{
    FCoreDelegates::OnControllerConnectionChange.AddUObject(this, &UGamepadManager::OnGamepadConnectionChanged);

    CheckConnectedGamepadsCount();
}

void UGamepadManager::CheckConnectedGamepadsCount()
{
    //hack for windows editor
#if PLATFORM_WINDOWS
	XINPUT_STATE XInputStates[MAX_NUM_XINPUT_CONTROLLERS];

	for (int32 ControllerIndex = 0; ControllerIndex < MAX_NUM_XINPUT_CONTROLLERS; ++ControllerIndex)
	{
		XINPUT_STATE& XInputState = XInputStates[ControllerIndex];
        FMemory::Memzero( &XInputState, sizeof(XINPUT_STATE) );

        bool bIsConnected = ( XInputGetState( ControllerIndex, &XInputState ) == ERROR_SUCCESS ) ? true : false;

        if (bIsConnected)
        {
            ActiveGamepadsCount++;
        }
	}
#endif

}

void UGamepadManager::OnGamepadConnectionChanged(bool Connected, int32 PlatformUserId, int32 ControllerIndex)
{
    if (Connected)
    {
        ActiveGamepadsCount++;
    }
    else
    {
        ActiveGamepadsCount--;
    }
}

int32 UGamepadManager::GetActiveGamepadsCount() const
{
    return ActiveGamepadsCount;
}