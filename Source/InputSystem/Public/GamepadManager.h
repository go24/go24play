#pragma once

#include <CoreMinimal.h>
#include <UObject/UObjectGlobals.h>

#include "InputSystem/Public/GamepadManagerInterface.h"

#include "GamepadManager.generated.h"

UCLASS()
class INPUTSYSTEM_API UGamepadManager : public UObject, public IGamepadManagerInterface
{
	GENERATED_BODY()

public:
    UGamepadManager(const FObjectInitializer& ObjectInitializer);

	int32 GetActiveGamepadsCount() const override;

protected:
	void CheckConnectedGamepadsCount();
	void OnGamepadConnectionChanged(bool Connected, int32 PlatformUserId, int32 ControllerIndex);

private:
	int32 ActiveGamepadsCount = 0;
};
