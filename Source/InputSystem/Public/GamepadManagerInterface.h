#pragma once

#include <Interface.h>
#include "BaseTypes/Public/PlayerInfo.h"

#include "GamepadManagerInterface.generated.h"

class IPlayersLobbyInterface;

UINTERFACE(BlueprintType)
class INPUTSYSTEM_API UGamepadManagerInterface : public UInterface
{
	GENERATED_UINTERFACE_BODY()
};

class INPUTSYSTEM_API IGamepadManagerInterface
{
	GENERATED_IINTERFACE_BODY()

public:
	virtual int32 GetActiveGamepadsCount() const = 0;
};
