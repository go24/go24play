using UnrealBuildTool;

public class InputSystem : ModuleRules
{
    public InputSystem(ReadOnlyTargetRules Target) : base(Target)
    {
        PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

        PublicIncludePaths.AddRange(new string[]
        {
            "InputSystem/Public"
        });

        PrivateIncludePaths.AddRange(new string[]
        {
            "InputSystem/Private"
        });

        PublicDependencyModuleNames.AddRange(new string[]
        {
            "BaseTypes"
        });

        PrivateDependencyModuleNames.AddRange(new string[]
        {
            "Core",
            "InputCore",
            "InputDevice",
            "CoreUObject",
            "Engine"
        });

        if ((Target.Platform == UnrealTargetPlatform.Win64) ||
			(Target.Platform == UnrealTargetPlatform.Win32))
		{
			AddEngineThirdPartyPrivateStaticDependencies(Target,
				"XInput"
			);
		}
    }
}