using UnrealBuildTool;

public class GameCore : ModuleRules
{
    public GameCore(ReadOnlyTargetRules Target) : base( Target )
    {
        PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

        bEnableUndefinedIdentifierWarnings = false;

        PublicIncludePaths.AddRange(new string[]
        {
            "GameCore/Public"
        });

        PrivateIncludePaths.AddRange(new string[]
        {
            "GameCore/Private"
        });


        PublicDependencyModuleNames.AddRange(new string[] 
        {
			"Player",
            "BaseTypes",
            "Interfaces",
            "MenuMatching",
            "Helpers",
            "Config",
            "InputSystem",
            "GameInstance",
            "GameLiftServerSDK"
        });

        PrivateDependencyModuleNames.AddRange(new string[]
        {
            "Core",
            "InputCore",
            "CoreUObject",
            "Engine",
            "TeamColor"
        });
    }
}
