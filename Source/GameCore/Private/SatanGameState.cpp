// Fill out your copyright notice in the Description page of Project Settings.


#include "SatanGameState.h"

#include "GameInstance/Public/ProjectGameInstance.h"

#include <Misc/CoreDelegates.h>
#include <Engine/World.h>
#include <GameFramework/PlayerController.h>
#include <Engine/LocalPlayer.h>

void ASatanGameState::PostInitializeComponents()
{
    Super::PostInitializeComponents();

    FCoreDelegates::OnControllerConnectionChange.AddUObject(this, &ASatanGameState::OnGamepadConnectionChanged);
}

void ASatanGameState::OnGamepadConnectionChanged(bool Connected, int32 PlatformUserId, int32 ControllerIndex)
{
    if (!Connected)
    {
        UProjectGameInstance* ProjectGameInstance = Cast<UProjectGameInstance>(GetWorld()->GetGameInstance());
        if (ProjectGameInstance)
        {
            ULocalPlayer* LocalPlayer = ProjectGameInstance->FindLocalPlayerFromControllerId(ControllerIndex);
            if (LocalPlayer)
            {
                ProjectGameInstance->RemoveLocalPlayer(LocalPlayer);
            }
        }
    }
}