
#include "SatanGameMode.h"

#include "PlayerManagerInterface.h"
#include "Helpers/InputHelper/InputHelper.h"
#include "GameInstance/Public/ProjectGameInstance.h"
#include "PlayerController/WarshipController.h"
#include "TeamColor/ColorGenerator/ColorGenerator.h"

#include <Kismet/GameplayStatics.h>
#include <Engine/World.h>
#include <GameFramework/PlayerController.h>
#include <Engine/LocalPlayer.h>
#include <GameFramework/PlayerStart.h>
#include <EngineUtils.h>

#if WITH_GAMELIFT
#include "GameLiftFPSGameMode.h"
#include "GameLiftFPS.h"
#include "Engine.h"
#include "EngineGlobals.h"
#include "GameLiftFPSHUD.h"
#include "GameLiftFPSCharacter.h"
#include "GameLiftServerSDK.h"
#endif // WITH_GAMELIFT

ASatanGameMode::ASatanGameMode()
    : Super()
{
    DebugFromBattleMap = true;

#if WITH_GAMELIFT

    FGameLiftServerSDKModule* gameLiftSdkModule = &FModuleManager::LoadModuleChecked<FGameLiftServerSDKModule>(FName("GameLiftServerSDK"));

    gameLiftSdkModule->InitSDK();

    auto onGameSession = [=](Aws::GameLift::Server::Model::GameSession gameSession)
    {
        gameLiftSdkModule->ActivateGameSession();
    };

    FProcessParameters* params = new FProcessParameters();
    params->OnStartGameSession.BindLambda(onGameSession);

    params->OnTerminate.BindLambda([=]() {gameLiftSdkModule->ProcessEnding(); });
    params->OnHealthCheck.BindLambda([]() {return true; });
    params->port = 7777;

    TArray<FString> logfiles;
    logfiles.Add(TEXT("aLogFile.txt"));
    params->logParameters = logfiles;

    gameLiftSdkModule->ProcessReady(*params);
#endif

    PrimaryActorTick.bCanEverTick = true;
}

void ASatanGameMode::PostInitializeComponents()
{
    UWorld* InWorld = GetWorld();
    UProjectGameInstance* ProjectGameInstance = Cast<UProjectGameInstance>(InWorld->GetGameInstance());
    if (ProjectGameInstance)
    {
        //get copy for changing next time
        LobbyInfo = ProjectGameInstance->GetLobbyInfo();
        LobbyPlayersInfo = ProjectGameInstance->GetLobbyPlayersInfo();
    }

    Super::PostInitializeComponents();
}

void ASatanGameMode::Tick(float DeltaSeconds)
{
    Super::Tick(DeltaSeconds);

    
    if (StartCheckPlayersReady && !GameInitialized)
    {
        bool AllPlayersReady = true;
        for (int32 i = 0; i < LobbyPlayersInfo.Num(); i++)
        {
            const FPlayerInfo& PlayerInfo = LobbyPlayersInfo[i];
            if (!IsValid(PlayerInfo.OwnedPlayerController))
            {
                AllPlayersReady = false;
            }
        }

        if (ArenaIsReady && (AllPlayersReady || Time <= 0.f))
        {
            InitPlayers();
            GameInitialized = true;
        }

        Time -= DeltaSeconds;
    }
}

void ASatanGameMode::InitializeArenaSize()
{

}

void ASatanGameMode::InitializeSpawnPoints(const FVector2D& SpaceShipSize)
{
    
}

void ASatanGameMode::InitPlayers()
{
    UE_LOG(LogTemp, Warning, TEXT("InitPlayers"));
    
    AColorGenerator* ColorGenerator = GetWorld()->SpawnActor<AColorGenerator>();
    ColorGenerator->AddToRoot();
    ColorGenerator->GenerateTeamColors(LobbyInfo.MaxTeamNumber);

    UWorld* InWorld = GetWorld();
    AGameModeBase* GameModePtr = InWorld->GetAuthGameMode();
    TSubclassOf<APawn> WarshipPawn = GameModePtr->DefaultPawnClass;

    TActorIterator<APlayerStart> PlayerStartIterator(InWorld, APlayerStart::StaticClass());

    for (int32 i = 0; i < LobbyPlayersInfo.Num(); i++)
    {
        const FPlayerInfo& PlayerInfo = LobbyPlayersInfo[i];

        APlayerController* PlayerController = PlayerInfo.OwnedPlayerController;
        if (PlayerController)
        {
            if (PlayerStartIterator)
            {
                APlayerStart* PlayerStart = Cast<APlayerStart>(*PlayerStartIterator);
                if (PlayerStart)
                {
                    const FVector Location = PlayerStart->GetActorLocation();
                    const FRotator Rotation = (FVector(0) - Location).Rotation();
                    
                    AWarshipPawn* Warship = InWorld->SpawnActor<AWarshipPawn>(WarshipPawn, Location, Rotation);
                    if (Warship)
                    {
                        PlayerStart->PlayerStartTag = FName("Busy");
                        PlayerController->Possess(Warship);
                        Warship->SetOwner(PlayerController);
                        Warship->SetColor(ColorGenerator->MatchColorBy(PlayerInfo.TeamId));
                    }
                }
                ++PlayerStartIterator;
            }
        }
    }

    ColorGenerator->RemoveFromRoot();
}

void ASatanGameMode::InitGame(const FString& MapName, const FString& Options, FString& ErrorMessage)
{
    UE_LOG(LogTemp, Warning, TEXT("InitGame"));
    Super::InitGame(MapName, Options, ErrorMessage);
    DebugFromBattleMap = Options.Find("FromMenu") == -1;
    PostArenaSetup.AddUObject(this, &ASatanGameMode::ArenaReady);
}

void ASatanGameMode::ArenaReady()
{
    ArenaIsReady = true;
}

void ASatanGameMode::PreLogin(const FString& Options, const FString& Address, const FUniqueNetIdRepl& UniqueId, FString& ErrorMessage)
{
    Super::PreLogin(Options, Address, UniqueId, ErrorMessage);
}

void ASatanGameMode::PostLogin(APlayerController* NewPlayer)
{
#if (PLATFORM_ANDROID || PLATFORM_IOS)
    Super::GenericPlayerInitialization(NewPlayer);
#endif
    StartCheckPlayersReady = true;

    AWarshipController* WarshipController = Cast<AWarshipController>(NewPlayer);
    if (WarshipController)
    {
        WarshipController->Client_SendBackYourClientID();
    }
    
    //Super::PostLogin(NewPlayer);
}

void ASatanGameMode::ClientIsReadyToSpawn(APlayerController* NewPlayer, int32 InClientID)
{
    if (NewPlayer)
    {
        for (int32 i = 0; i < LobbyPlayersInfo.Num(); i++)
        {
            const FPlayerInfo& PlayerInfo = LobbyPlayersInfo[i];
            if (PlayerInfo.UniquePlayerId == InClientID && PlayerInfo.PlayerId == NewPlayer->NetPlayerIndex)
            {
                LobbyPlayersInfo[i].OwnedPlayerController = NewPlayer;
            }
        }
    }
}