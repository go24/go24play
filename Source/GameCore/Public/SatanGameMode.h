#pragma once

#include <CoreMinimal.h>
#include <GameFramework/GameMode.h>
#include <Engine/BlockingVolume.h>
#include <Delegate.h>

#include "Templates/SubclassOf.h"
#include "SatanGameModeBase.h"
#include "PlayerInfo.h"

#include "SatanGameMode.generated.h"

DECLARE_MULTICAST_DELEGATE(FPostArenaSetup);

UCLASS()
class GAMECORE_API ASatanGameMode : public ASatanGameModeBase
{
	GENERATED_BODY()

public:
    ASatanGameMode();

    void PostInitializeComponents() override;

    void Tick(float DeltaSeconds) override;

    void ClientIsReadyToSpawn(APlayerController* NewPlayer, int32 InClientID);

    UPROPERTY(BlueprintReadOnly, Category = "SatanGameMode\LevelManager")
    FVector2D PointAA; 

    UPROPERTY(BlueprintReadOnly, Category = "SatanGameMode\LevelManager")
    FVector2D PointBB; 

    UPROPERTY(BlueprintReadOnly, Category = "SatanGameMode\LevelManager")
    FVector2D ArenaSize;

    UPROPERTY(BlueprintReadOnly, Category = "SatanGameMode\LevelManager")
    bool IsSuccessful = false;

    FPostArenaSetup PostArenaSetup;

protected:
    virtual void PostLogin(APlayerController* NewPlayer) override;
    void PreLogin(const FString& Options, const FString& Address, const FUniqueNetIdRepl& UniqueId, FString& ErrorMessage) override;

    void InitGame(const FString& MapName, const FString& Options, FString& ErrorMessage) override;

public:
    void InitPlayers();

    void ArenaReady();

    bool DebugFromBattleMap;

    UFUNCTION(BlueprintCallable, Category = "SatanGameMode\ArenaSetupComponent")
    void InitializeArenaSize();

    UFUNCTION(BlueprintCallable, Category = "SatanGameMode\ArenaSetupComponent")
    void InitializeSpawnPoints(const FVector2D& SpaceShipSize);


private:
    UPROPERTY()
    FLobbyInfo LobbyInfo;

    UPROPERTY()
    TArray<FPlayerInfo> LobbyPlayersInfo;

    //utility fields
    bool StartCheckPlayersReady = false;
    float Time = 30.f;
    bool GameInitialized = false;
    bool ArenaIsReady = false;
};

// void AGameMode24::HandleMatchIsWaitingToStart()
// {
//     //GEngine->AddScreenDebugMessage(-1, 5.0f, FColor::Red, TEXT("Waiting to start"));
// }