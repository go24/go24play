// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include <CoreMinimal.h>
#include <GameFramework/GameState.h>

#include "SatanGameState.generated.h"

/**
 * 
 */
UCLASS()
class GAMECORE_API ASatanGameState : public AGameStateBase
{
	GENERATED_BODY()
	
public:
	void PostInitializeComponents() override;


protected:
	void OnGamepadConnectionChanged(bool Connected, int32 PlatformUserId, int32 ControllerIndex);
};
